import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { FinpayPaiementPage } from '../finpay-paiement/finpay-paiement';
import { QrScannerProvider } from '../../providers/qr-scanner/qr-scanner'

/**
 * Generated class for the QrScannerPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-qr-scanner',
  templateUrl: 'qr-scanner.html',
})
export class QrScannerPage {

  booking$: string;
  reservation$: string;
  params: any;
  constructor(
  	public navCtrl: NavController, 
  	public navParams: NavParams,
  	private qrscannerProvider: QrScannerProvider
  ) {
  	// this.init();
  }
  
  ngOnInit(){
      this.init();
  }

  init(){
  	this.booking$ = this.navParams.get('booking$');
  	this.reservation$ = this.navParams.get('reservation$');
    this.params = this.navParams.get('params')

  	this.prepareScanner();
  }

  prepareScanner(){
  	this.qrscannerProvider.openScaner(FinpayPaiementPage,this.params);
  }

  ionViewDidLoad() {
    
  }

}
