import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { LoginFinpayPage } from './login-finpay';

@NgModule({
  declarations: [
    LoginFinpayPage,
  ],
  imports: [
    IonicPageModule.forChild(LoginFinpayPage),
  ],
})
export class LoginFinpayPageModule {}
