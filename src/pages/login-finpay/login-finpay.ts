import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams,ToastController, MenuController, Alert, LoadingController, AlertController, Loading } from 'ionic-angular';

import { FormGroup, Validators, FormBuilder } from '@angular/forms';

import { ConfirmPasswordFinpayPage } from '../confirm-password-finpay/confirm-password-finpay';

import { PopUpProvider } from '../../providers/pop-up/pop-up';


//////////////////////////

import { Credential } from '../../lib/credential';
import { AuthProvider } from '../../providers/cyclos/auth';
import { Session } from '../../lib/models/session';
import { Storage } from '@ionic/storage';
import { Config } from '../../config/config';
import { CyclosProvider } from '../../providers/cyclos/cyclos';
import { OrchidProvider } from '../../providers/orchid/orchid';
import { CustomValues } from '../../lib/models/customValues';
import { Constant } from '../../constant';

import { RegisterFinpayPage } from '../register-finpay/register-finpay'



/**
 * Generated class for the LoginFinpayPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-login-finpay',
  templateUrl: 'login-finpay.html',
})
export class LoginFinpayPage {
 
 	form: FormGroup;
 	params: any;
  alertConf: Alert;

  protected secondPage: any = ConfirmPasswordFinpayPage ;
  config = Config;
  test :any ;

  constructor(public navCtrl: NavController,
   public navParams: NavParams,
   private formBuilder: FormBuilder,
   private popupProvider: PopUpProvider,
    public auth: AuthProvider,
    private storage: Storage, 
    private orchidPrv: OrchidProvider,
    public toastCtrl: ToastController,
    public alertCtrl: AlertController,
    private loadingCtrl: LoadingController,
    public menuCtrl: MenuController
    ) {

     //super(toastCtrl);

  	this.form = this.formBuilder.group({
 			login: ['',Validators.required],
 			password: ['',Validators.required]
 		});

	  this.params = this.navParams.get('params');
    console.log(this.params);

   this.test =  window.localStorage.getItem('deviceId');
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad LoginFinpayPage');
  }

  onSubmit() {
    /*if (this.form.valid) {
      this.navCtrl.push(ConfirmPasswordFinpayPage,{params: this.params,username :this.form.value.login,password: this.form.value.password });  
    }else{
      this.popupProvider.PresentToast('tous les champs sont obligatoire');
    }*/
    if (this.form.valid) {
      let value = this.form.value;
      if (this.form.value.login)
        value.name = this.form.value.login;
      let credential = new Credential(value);
      let loading = this.presentLoading();
      this.auth.login(credential).then((session: Session) => {
        CyclosProvider.session = session;
        console.log(session)
        /* validation account orchid-cyclos*/ 
        this.auth.isExistIdOrhid(credential).then((customValues :CustomValues) => {
          //console.log(customValues)
          //console.log("compte validé")
          this.setStorage(session,loading)
          //this.alert("Compte déja validé")
        }).catch((customValues:CustomValues) => {
          console.log("in erreur")
          this.orchidPrv.isExistAccount(customValues.cin).then((account:any) => {
              console.log(account.idOrchid)
              if(account.status) {
                console.log("ok")
                this.auth.validateAccount(credential, account.idOrchid).then((success) => {
                  console.log(success)
                  this.alert("Votre compte a bien été validé")
                  this.setStorage(session,loading)
                  //loading.dismiss();
                }).catch(error => {
                  //console.log(error)
                  //console.log("Erreur lors de la validation de votre compte")
                  this.alert("Erreur lors de la validation de votre compte")
                  loading.dismiss();
                })
              }else {
                //console.log("Votre compte est en cours de validation")
                this.alert("Votre compte est en cours de validation")
                loading.dismiss();
              }
            }).catch((error) => {
              //console.log(error)
              this.alert("vous n'avez pas encore de compte créez-en un")
              loading.dismiss();
          })
        })
        /* validation account orchid-cyclos */ 
      }).catch((error) => {
        console.error(error);
        //this.alert("Votre login")
        loading.dismiss();
      })
    } else {
      //this.reportError();
    }
  }


  protected rootToSecondPage() {
    
    this.navCtrl.push(this.secondPage, {
           params: this.params
          })
    
  }

  protected register(){
    this.navCtrl.push(RegisterFinpayPage,{
           params: this.params
          })
  }

  protected session() {
    return new Promise((resolve) => {
      this.storage.get(Config.SESSION_STO).then((session: Session) => {
        if (!session) {
          resolve()
          return
        } 

        CyclosProvider.session = session;

        this.auth.isNotExpired(session).then(() => {
          this.rootToSecondPage();
          resolve()
        }).catch(() => {
          resolve()
        })
        // this.auth.allowed().then((auth: Session) => {
        //   if (auth.permissions.banking) {
        //     this.rootToSecondPage();
        //     resolve();
        //   } else {
        //     resolve();
        //   }
        // }, () => {
        //   resolve();
        // }).catch((error) => {
        //   resolve();
        // });

      }).catch(() => {
        resolve();
      });
    })
  }


  ionViewCanEnter() {
    this.menuCtrl.enable(true)
    return this.session();
  }

  protected presentLoading(): Loading {
    let loading: Loading = this.loadingCtrl.create({
      content: Constant.loading_label
    });
    loading.present();
    return loading;
  }

  private setStorage(session,loading) {
    this.storage.set(Config.SESSION_STO, session).then(() => {
      loading.dismiss().then(() => {
        this.rootToSecondPage();
      })
    })
      /*
        CyclosProvider.session = session;
        this.events.publish(Config.LOGIN_EVENT);
      */
  }

  public getMessageError(key: string, validator: string) {
    return Constant.error_empty_field;
  }

  private alert(message:any,title?:any) {
    this.alertCtrl.create({
      title: title,
      message: message,
      buttons: [{
        text: 'OK'
        }]
    }).present()
  }


}
