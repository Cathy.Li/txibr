import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { CooperativeProvider } from '../../providers/cooperative/cooperative';
import { Cooperative } from '../../models/cooperative.model';
import { PopUpProvider } from '../../providers/pop-up/pop-up';
import { BrokerRequestProvider } from '../../providers/broker-request/broker-request';
import { BroKerRequest } from '../../models/broker-request.model';
import { FacebookProvider } from '../../providers/facebook/facebook';
import { Profile } from '../../models/profile.model';

import { CooperativeBrokerRegisterPage } from '../cooperative-broker-register/cooperative-broker-register';

/**
 * Generated class for the CooperativeBrokerPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-cooperative-broker',
  templateUrl: 'cooperative-broker.html',
})
export class CooperativeBrokerPage {

  user: any;
  customer$: string;
  cooperatives = [];
  requester: Profile = new Profile();
  constructor(
  	public navCtrl: NavController, 
  	public navParams: NavParams,
  	private cooperativeProvider: CooperativeProvider,
  	private popupProvider: PopUpProvider,
  	private requestProvider: BrokerRequestProvider,
  	private facebookProvider: FacebookProvider
  	) {
  	
  }

  ngOnInit(){
      this.customer$ = this.navParams.get('customer$');
    this.popupProvider.showLoading();
    this.init();
  }


  init(){
  	this.cooperativeProvider.fetcAll().subscribe((cooperatives : Cooperative)=>{
  		this.cooperatives = [];
  		for(let key in cooperatives){
  			cooperatives[key].key = key;
  			this.cooperatives.push(cooperatives[key]);
  			this.popupProvider.dismissLoading();
  		}
  	})
  }

  /*ionViewDidLoad() {
    this.facebookProvider.getUser().then((profile)=>{
      this.user = profile;
      this.customer$ = this.user['id'];
    });
  }*/

  sendrequest(cooperative){
    /*this.popupProvider.presentConfirm('Voulez-vous envoyer la démande à ' + cooperative.name + ' ?').then((ok)=>{
  		this.requestProvider.customPath(`cooperative/${cooperative.key}/broker_request`);
  		//this.requestProvider.save(new BroKerRequest({name:this.requester.name}), this.customer$ );
      this.requestProvider.save(new BroKerRequest({name:this.user['name']}), this.customer$ );
  	}, (cancel)=>{})*/

    this.navCtrl.push(CooperativeBrokerRegisterPage, {params: cooperative, customer$: this.customer$});
  }

}
