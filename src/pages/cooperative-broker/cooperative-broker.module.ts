import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CooperativeBrokerPage } from './cooperative-broker';

@NgModule({
  declarations: [
    CooperativeBrokerPage,
  ],
  imports: [
    IonicPageModule.forChild(CooperativeBrokerPage),
  ],
})
export class CooperativeBrokerPageModule {}
