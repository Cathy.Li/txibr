import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams  } from 'ionic-angular';
import { PopUpProvider } from '../../providers/pop-up/pop-up';
import { CarProvider } from '../../providers/car/car';
import { Car } from '../../models/car.model';
import { CarType } from '../../models/car-type.model';
import { CarTypeProvider } from '../../providers/car-type/car-type';
import { SeatPickerPage } from '../seat-picker/seat-picker';
import { PlanningProvider } from '../../providers/planning/planning';
import { EventProvider } from '../../providers/event/event'

@IonicPage()
@Component({
   selector: 'page-available-car',
   templateUrl: 'available-car.html',
})
export class AvailableCarPage {

   class: string;
   path: string;
   cars = [];
   coop: string;
   price: any;
   params: any;
   customer$: string;
   request: any;

   constructor(
      private navCtrl: NavController,
      private navParams: NavParams,
      private popupProvider: PopUpProvider,
      private carProvider: CarProvider,
      private cartypeProvider: CarTypeProvider,
      private planningProvider: PlanningProvider,
      private eventProvider: EventProvider

      ){	
      /*this.request = this.navParams.get('request');
      console.log(this.request);
      this.init();
      this.customer$ = this.navParams.get('customer$');
      this.eventProvider.setEvent('customer$',{customer$: this.customer$});*/
   }


  ngOnInit(){
       this.request = this.navParams.get('request');
      this.init();
      this.customer$ = this.navParams.get('customer$');
      this.eventProvider.setEvent('customer$',{customer$: this.customer$});
  }



   init(){
      this.params = this.navParams.get('params');
      this.class = this.params.class;
      this.path = this.params.path;
      this.coop = this.params.coop;
      this.price = this.params.price;
      this.planningProvider.customPath(this.path);



      this.planningProvider.fetch(this.class).then((planning)=>{
         if (planning) {
            this.popupProvider.showLoading();

            //let cars = JSON.parse(planning['cars']);
            let cars = planning['cars'];
            let cpath = `cooperative/${this.coop}/car`
            this.carProvider.customPath(cpath);
            for(let c in cars){

               for (let ca in cars[c]) {
                  this.carProvider.fetch(cars[c][ca]).then((car: Car)=>{
                     this.cartypeProvider.fetch(car.cartype).then((type: CarType)=>{
                        let _car = {
                           matricule: car.matricule,
                           cartype: type
                        }
                        this.cars.push(_car);
                     })
                  })
               }
               

            }
            this.popupProvider.dismissLoading()
         }
      })
   }

   selectCar(matricule){
      let params = this.params;
      params.matricule = matricule;
      this.navCtrl.push(SeatPickerPage, { params: params, request: this.request })
   }

}
