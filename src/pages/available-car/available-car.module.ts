import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AvailableCarPage } from './available-car';

@NgModule({
  declarations: [
    AvailableCarPage,
  ],
  imports: [
    IonicPageModule.forChild(AvailableCarPage),
  ],
})
export class AvailableCarPageModule {}
