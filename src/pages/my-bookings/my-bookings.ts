import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { StationProvider } from '../../providers/station/station';
import { Station } from '../../models/station.model';
import { BookingDetailsPage } from '../booking-details/booking-details';
import { PlanningProvider } from '../../providers/planning/planning';
import { CooperativeProvider } from '../../providers/cooperative/cooperative';
import { TrajectProvider } from '../../providers/traject/traject';
import { PopUpProvider } from '../../providers/pop-up/pop-up';


import { Cooperative } from '../../models/cooperative.model';
import { Planning } from '../../models/planning.model';
	import * as moment from 'moment';


/**
 * Generated class for the MyBookingsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

 @IonicPage()
 @Component({
 	selector: 'page-my-bookings',
 	templateUrl: 'my-bookings.html',
 })
 export class MyBookingsPage {

 	customer$: string;
 	bookings = [];
 	constructor(
 		public navCtrl: NavController, 
 		public navParams: NavParams,
 		private stationProvider: StationProvider,
 		private planningProvider: PlanningProvider,
 		private cooperativeProvider: CooperativeProvider,
 		private trajectProvider: TrajectProvider,
 		private popupProvider: PopUpProvider
 		) {
 		this.popupProvider.showLoading();
 		// this.init()
 	}

 	ngOnInit(){
	    this.init();
	}

 	init(){
 		this.customer$ = this.navParams.get('customer$');
 		let uid = this.customer$;
 		
 		// Alvin 
 		/*this.customer$ = "2241575522759918";
 		let uid = "2241575522759918";*/


 		this.cooperativeProvider.fetcAll().subscribe((cooperatives: Cooperative)=>{

 			this.bookings = [];
 			for(let c in cooperatives){
 				let ppath = `cooperative/${c}/planning`;
 				this.planningProvider.customPath(ppath);
 				this.planningProvider.fetcAll().subscribe((plannings: Planning)=>{
 					for(let p in plannings){
 						for(let h in plannings[p]){
 							for(let t in plannings[p][h]){
 								
 								let tpath = `cooperative/${c}/trajet`;
 								this.trajectProvider.customPath(tpath);
 								this.trajectProvider.fetch(t).then((traject)=>{
									if(traject){
	 									this.stationProvider.fetch(traject['depart']).then((startstation: Station)=>{
	 										this.stationProvider.fetch(traject['arrive']).then((arrivalstation: Station)=>{

	 											let trajet = {};
	 											trajet = traject;
	 											trajet['startstation'] = startstation;
	 											trajet['arrivalstation'] = arrivalstation;
	 											for(let bc in plannings[p][h][t]){
	 												let reservations = [];
	 												if ( plannings[p][h][t][bc]['reservation']) {
	 													reservations =  plannings[p][h][t][bc]['reservation'][uid];

	 													for(let r in reservations){
	 														let booking = {};
	 														booking['cooperative'] = cooperatives[c];
	 														booking['reservation'] = reservations[r];
	 														//booking['coop_phone'] = cooperatives[c]['telContact'];
	 														booking['trajet'] = trajet;
	 														booking['params'] = {
	 															class: bc,
	 															day: p,
	 															hour: h,
	 															path: `cooperative/${c}/planning/${p}/${h}/${t}`,
	 															coop: c,
	 															trajet: trajet,
	 															date: reservations[r].date,
	 															reservation$: r,
	 															phone_coop: cooperatives[c]['telContact'],
	 															uid: uid,
	 															matricule: reservations[r].car

	 														}

	 														this.bookings.push(booking);
	 													}
	 												} 									
	 											}

	 										})

	 									})

	 								}
 								})
 							}
 						}					
 					}
 				});
 			}
 		});
 		
		this.popupProvider.dismissLoading();
 		//console.log(this.bookings);
 	}

 	showDetails(params){
 		this.navCtrl.push(BookingDetailsPage, {params: params});

 	}


 }
