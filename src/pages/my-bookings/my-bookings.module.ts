import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MyBookingsPage } from './my-bookings';

import { AngularFireModule } from "angularfire2";
import { FIREBASE_CONFIG } from '../../app/app.firebase.config';

@NgModule({
  declarations: [
    MyBookingsPage,
  ],
  imports: [
    IonicPageModule.forChild(MyBookingsPage)
  ],
})
export class MyBookingsPageModule {}
