import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ConfirmPasswordFinpayPage } from './confirm-password-finpay';

@NgModule({
  declarations: [
    ConfirmPasswordFinpayPage,
  ],
  imports: [
    IonicPageModule.forChild(ConfirmPasswordFinpayPage),
  ],
})
export class ConfirmPasswordFinpayPageModule {}
