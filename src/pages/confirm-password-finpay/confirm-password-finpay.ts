import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController, Loading , AlertController} from 'ionic-angular';


import { FormGroup, Validators, FormBuilder } from '@angular/forms';

import { StatusPaiementPage } from '../status-paiement/status-paiement';
import { PopUpProvider } from '../../providers/pop-up/pop-up';
import { StorageProvider } from '../../providers/storage/storage';
import { Session } from '../../lib/models/session';



import { CarProvider } from '../../providers/car/car';
import { ReservationProvider } from '../../providers/reservation/reservation';
import { CooperativeProvider } from '../../providers/cooperative/cooperative'
import { Car } from '../../models/car.model';
import { Reservation } from '../../models/reservation.model';
import { PaiementMethodPage } from '../paiement-method/paiement-method';
import { PrinterWidgetPage } from '../printer-widget/printer-widget';
import { BookingClassProvider } from '../../providers/booking-class/booking-class';
import { EventProvider } from '../../providers/event/event';


import { CyclosProvider } from '../../providers/cyclos/cyclos';
import { AccountProvider } from '../../providers/cyclos/account';
import { Credential } from '../../lib/credential';
import { Payment, PaymentType, CustomValues } from '../../lib/models/payment';
import { Invoice } from '../../lib/models/invoice';
import { PaymentProvider } from '../../providers/cyclos/payment';
import { Qrcode } from '../../lib/models/qrcode';
//import { FirebaseProvider } from '../../providers/firebase/firebase';
import { Config } from '../../config/config'
import { OrchidProvider } from '../../providers/orchid/orchid';
import { Transfer } from '../../lib/models/transfer';


import { Account } from '../../lib/models/account';
import { AccountDetailPage } from '../account-detail/account-detail';
import { UserProvider } from '../../providers/cyclos/user';
import { AuthProvider } from '../../providers/cyclos/auth';
import { SaleProvider } from '../../providers/sale/sale';
import { QrcodeProvider } from '../../providers/qrcode/qrcode';
import { Sale } from '../../models/sale.model';
import { QRCode } from '../../models/qrcode.model';
import { BrokerProvider } from '../../providers/broker/broker';
import { Broker } from '../../models/broker.model';
import { BrokerRequestProvider } from '../../providers/broker-request/broker-request';
import { BillingProvider } from '../../providers/billing/billing';
import { UtilProvider } from '../../providers/util/util';
import { PlaceProvider } from '../../providers/place/place';
/**
 * Generated class for the ConfirmPasswordFinpayPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-confirm-password-finpay',
  templateUrl: 'confirm-password-finpay.html',
})
export class ConfirmPasswordFinpayPage {
	form: FormGroup;
	params:any;
  booking$: string;
  reservation$: string;
  paid: boolean;
  cooperative$: string;
  car$: string;
  details = {} ;
  qrcode: string;

  invoice: Invoice = new Invoice();
  loading: Loading;
  credential:Credential;
  session : Session;
  idAgent: string;
  idTransaction: string;
  availableBalance: string
  currency: string;
  numTel : string;
  users :string;
   user:any = {
     length: false
   };

  amountTransfered : number;
  amount : number;

  customValues : CustomValues;
  IdOrchiDest  : string;

  reservation: Reservation = new Reservation();
  broker: any;

  avanceTransfered: boolean;


  reservatio: any;
  bill: any;
  qrCode: any;
  reserver: any;
  sale: any;


  	constructor(public navCtrl: NavController, 
      public navParams: NavParams,
      private formBuilder: FormBuilder,
      private popupProvider: PopUpProvider,
      private storagePrvd:StorageProvider,
      private carProvider: CarProvider,
      private reservationProvider: ReservationProvider,
      private cooperativeProvider: CooperativeProvider,
      private bookingclassProvider: BookingClassProvider,
      private eventProvider: EventProvider,
      private accountPrvd: AccountProvider,
      private paymentPrvd: PaymentProvider, 
      private loadingCtrl: LoadingController,
      private orchidPrvd: OrchidProvider,
      private alertCtrl: AlertController,
      public accountPrd: AccountProvider,
      private userPrvd: UserProvider,
      public auth: AuthProvider,
      public saleProvider: SaleProvider,
      public qrcodeProvider: QrcodeProvider,
      private brokerProvider: BrokerProvider,
      private requestProvider: BrokerRequestProvider,
      public billingProvider: BillingProvider,
      public utilProvider: UtilProvider,
      private placeProvider: PlaceProvider
      ) {
  		this.form = this.formBuilder.group({
   			passwordconfirm: ['',Validators.required]
   		});
      /*this.avanceTransfered = false;

  	this.params = this.navParams.get('params');
    this.init();*/
  	}

    
    ngOnInit() {
      this.avanceTransfered = false;

      this.params = this.navParams.get('params');
      this.init();
    }


    init(){
      if(this.params){
      if (this.params.reservation) {

        this.reservatio = this.params.reservation;
        this.bill = this.params.billing;
        this.qrCode = this.params.qrcode;
        this.sale = this.params.sale;
        this.reserver = this.params.reserver;


        this.eventProvider.setEvent('customer$',{customer$: this.params.uid});
        this.numTel = this.params.phone_coop;

        this.details['date'] = this.reservatio.reservation_date;
        this.details['trajet'] = this.params.trajet;
        this.details['hour'] = this.reservatio.reservation_hour;



        this.carProvider.customPath(`cooperative/${this.params.coop}/car`);
        this.carProvider.fetch(this.params.matricule).then((car: Car)=>{
          this.details['car'] = car;
        });

        this.details['reservation'] = {
          amount: this.reservatio.reservation_amount,
          nbplace: this.reservatio.reservation_nbplace,
          status: this.reservatio.reservation_status
        }

        this.cooperativeProvider.fetch(this.params.coop).then((cooperative)=>{
          this.details['cooperative'] = cooperative;

          let am = this.utilProvider.purcentageAgent(this.reservatio.reservation_amount,this.details['cooperative'].penaliteCanc);
          this.avanceTransfered = true;
          this.amountTransfered = am;

          this.details['client'] = this.bill.billing_data;
          this.broker = this.bill.billing_broker;

        });  
      }
      else{
        this.numTel = this.params.phone_coop;

        this.eventProvider.setEvent('customer$',{customer$: this.params.uid});
        this.bookingclassProvider.customPath(`cooperative/${this.params.coop}/booking_class`);
        this.bookingclassProvider.fetch(this.params.class).then((bookingclass)=>{
        this.details['class'] = bookingclass;
        this.details['reference'] = this.params.reservation$;
        this.details['trajet'] = this.params.trajet;
        this.details['date'] = this.params.date;
        this.details['hour'] = this.params.hour;
        this.cooperativeProvider.fetch(this.params.coop).then((cooperative)=>{
          this.details['cooperative'] = cooperative;

          let rpath = `${this.params.path}/${this.params.class}/reservation/${this.params.uid}`;
          this.reservationProvider.customPath(rpath);
          this.reservationProvider.fetch(this.params.reservation$).then((reservation: Reservation)=>{
            this.details['reservation'] = reservation;
            this.paid = false;
            this.amountTransfered = reservation['rest'];


            if (reservation.status == 'completed') {
              this.paid = true;
            }

            if (reservation['rest']===reservation['amount']) {
               let am = this.utilProvider.purcentageAgent(reservation['rest'],this.details['cooperative'].penaliteCanc);
              this.avanceTransfered = true;
              this.amountTransfered = am;
            }

            this.qrcode = reservation.qrcode;

            this.carProvider.customPath(`cooperative/${this.params.coop}/car`);
            this.carProvider.fetch(this.params.matricule).then((car: Car)=>{
              this.details['car'] = car;
            });

            this.brokerProvider.fetch(this.params.uid).then((broker: Broker)=>{ 
                if (broker) {
                  //broker for all

                  this.broker = broker;

                  this.billingProvider.customPath(`billing/${this.params.uid}`);
                  this.billingProvider.fetch(this.details['reservation'].billing).then((billing)=>{
                      if (billing) {
                        this.details['client'] = billing;
                      }
                  });
                }else{
                  //broker for cooperative
                  this.requestProvider.customPath(`cooperative/${this.params.coop}/broker_request`);
                  this.requestProvider.fetch(this.params.uid).then((broker: Broker)=>{
                    if (broker) {
                      // code...

                      this.broker = broker;

                      /*let am = this.utilProvider.purcentage(reservation['rest'],this.details['cooperative'].purcentBrokerC);

                      this.amountTransfered = am;*/

                      this.billingProvider.customPath(`billing/${this.params.uid}`);
                      this.billingProvider.fetch(this.details['reservation'].billing).then((billing)=>{
                        if (billing) {
                          this.details['client'] = billing;
                        }
                      });
                    }
                  })
                }
            })
          })
        });      
      })
    }
  }
   
  }


  ionViewDidLoad() {
    this.presentLoading()

    this.storagePrvd.getSession().then((session :Session) => {

      this.session = session;

      if (this.numTel) {
        let key = this.numTel;
        this.userPrvd.getUsersbycontact(this.session, key).then((user : any) => {  
           if (user.length >0) {
             this.user = user[0];
             //let test = this.user[0].customValues.orchid_account;
             this.users = this.user;
           }
           
        })
      }

      this.accountPrd.getAccountType(session).then((account:Account) => {
        //this.session = session
        this.currency =  account.currency.name
        this.availableBalance = account.status.availableBalance
        this.loading.dismiss()
      }).catch((error) => {
        console.log(error)
        this.loading.dismiss()
      })
    }).catch((error) => {
      console.log(error)
      this.loading.dismiss()
    })
  }



  account() {
    this.navCtrl.push(AccountDetailPage, { session: this.session , availableBalance : this.availableBalance})
  }

  protected presentLoading() {
    this.loading =  this.loadingCtrl.create()
    return this.loading.present()
  }

  

  //paiement compte à compte

   protected ConfirmTransfert() {
    this.session = CyclosProvider.session
    this.credential = new Credential({
      name: this.session.principal,
      password: this.user.password
    })
    this.auth.isExistIdOrhid(this.credential).then((customValues :CustomValues) =>{
      this.customValues = customValues;
      this.IdOrchiDest =  this.customValues['orchid_account'];
       console.log(this.customValues['orchid_account']);
    })

    this.accountPrvd.getAccountList(this.credential)
    .then((acccount) => {
    this.ConfirmTransfer()
   
    }, (error) => {
    this.alertCtrl.create({
        title: "Attention",
        message: "Mots de passe invalide",
        buttons: [{
          text: 'OK',
          handler: data => {
            this.loading.dismiss()
          }
        }]
      })
      .present()
    })
  }
 
  ConfirmTransfer() { 
    this.presentLoading();

    let transaction: Payment = {
      amount: this.amountTransfered,
      description: 'de',
      subject: this.user.id,
      type: PaymentType.USER_TRADE
    }
    //let test = this.IdOrchiDest
    //console.log(test)
    let transfer: Transfer = {
      creditedAccountId: this.user.customValues.orchid_account,
      debitedAccountId:this.IdOrchiDest ,
      amount: this.amountTransfered,
      wording: "Frais de transport "
    }
    //console.log(transfer)
 
    //console.log(transaction)
      this.credential = new Credential({
      name: this.session.principal,
      password: this.user.password
      })
      this.storagePrvd.getSession().then((session : Session) => {
        //console.log(session.user.id);
        this.paymentPrvd.setTransaction( transaction, this.session,).then((payment :any ) => {
            //console.log(payment);
          this.idTransaction = payment.id;
          this.orchidPrvd.setTransfer(transfer);
          if (this.params.reservation) { 
            this.create_reservation();
          } else {
            //set status 
            this.paiement_success();
            this.verifier_broker();
          }
          

          this.loading.dismiss();
          this.navCtrl.push(StatusPaiementPage ,{params: this.params,id : this.idTransaction});
        }).catch((error) => {
          this.loading.dismiss();
          console.log(error)
        })
      }).catch((error) => {
        console.log(error)
        this.loading.dismiss()
      }) 
  }

  paiement_success(){
    
     let path = `${this.params.path}/${this.params.class}/reservation/${this.params.uid}`;

     this.reservationProvider.customPath(path);

     this.reservationProvider.fetch(this.params.reservation$).then((reservation: Reservation)=>{
       this.reservation = reservation;
       this.reservation.status = 'completed';
       // if (reservation['rest']===reservation['amount']) {
       //     let am = this.utilProvider.purcentageAgent(reservation['rest'],this.details['cooperative'].penaliteCanc);
       //    this.reservation.rest = reservation['amount']-am;
       //    this.reservation.status = 'pending';
       //  }
       this.reservationProvider.save(this.reservation,this.params.reservation$);
     });
   }


   verifier_broker(){

     this.brokerProvider.fetch(this.params.uid).then((broker: Broker)=>{ 
        if (broker) {
                  //broker for all
            this.saleProvider.customPath(`broker/${this.params.uid}/sale`);
            this.saleProvider.fetcAll().subscribe((mySales: Sale[])=>{ 
              if (mySales) {
                for(let key in mySales){ 
                  let sale = mySales[key];
                  if (sale.status === false) {
                    this.qrcodeProvider.fetch(sale.qrReservation).then((qrcode: QRCode)=>{
                      if (qrcode && qrcode.uid === this.params.uid) {
                        sale.status = true;
                        this.saleProvider.customPath(`broker/${this.params.uid}/sale`);
                        this.saleProvider.save(sale,sale.qrReservation);
                      }
                    });
                  }
                }
              }
            });
        }else{
                  //broker for cooperative
          this.requestProvider.customPath(`cooperative/${this.params.coop}/broker_request`);
          this.requestProvider.fetch(this.params.uid).then((broker: Broker)=>{
            if (broker) {
              // code...
              this.saleProvider.customPath(`cooperative/${this.params.coop}/broker_request/${this.params.uid}/sale`);
              this.saleProvider.fetcAll().subscribe((mySales: Sale[])=>{ 
                if (mySales) {
                  for(let key in mySales){ 
                    let sale = mySales[key];
                    if (sale.status === false) {
                      this.qrcodeProvider.fetch(sale.qrReservation).then((qrcode: QRCode)=>{
                        if (qrcode && qrcode.uid === this.params.uid) {
                          sale.status = true;
                          this.saleProvider.customPath(`cooperative/${this.params.coop}/broker_request/${this.params.uid}/sale`);
                          this.saleProvider.save(sale,sale.qrReservation);
                        }
                      });
                    }
                  }
                }
              });
            }
          })
        }
    })

    
  }
 
 
    verifie() {
      const prompt = this.alertCtrl.create({
        title: "",
   
        message: "Saisir votre mot de passe pour confirmer",
        inputs: [
          {
            name: "uu",
            placeholder: 'Mot de passe',
            type : "password",
          },
        ],
        buttons: [
          {
            text: 'Annuler',
            handler: data => {
              console.log('Cancel clicked');
            }
          },
          {
            text: 'OK',
            handler: data => {
              this.user.password = data.uu
              this.ConfirmTransfert();
            }
          }
        ],
          cssClass: 'tb-alert'
      });
      prompt.present();
    }

  create_reservation(){

    let reserv = new Reservation();
    reserv.nbplace = this.reservatio.reservation_nbplace;
    reserv.placelist = this.reservatio.reservation_placelist;
    reserv.car = this.params.matricule;

    reserv.amount = this.reservatio.reservation_amount;
    reserv.rest = this.reservatio.reservation_amount-this.amountTransfered;
    reserv.status = this.reservatio.reservation_status;

    
    reserv.date = this.reservatio.reservation_date;
    reserv.qrcode = this.reservatio.reservation_qrcode;

    this.billingProvider.customPath(`${this.bill.billing_path}`);
    console.log(this.details['client'])
    let billing$ = String(this.billingProvider.save(this.details['client']));


    reserv.billing = billing$;
    reserv.customer = String(this.params.uid);



    this.reservationProvider.customPath(`${this.reservatio.reservation_path}`);
    let reservation$ = this.reservationProvider.save(reserv);

    this.add_qrCode(reservation$);
    this.add_place_reserved();

    if (this.bill.billing_broker) {
      this.add_sale();
    }
  }

  add_place_reserved(){
      this.placeProvider.customPath(`${this.reserver.reserver_path}`);
      this.placeProvider.save(this.reserver.reserver_place,this.reserver.reserver_hour);  
  }

  add_qrCode(reservation$){
     
    let qrcodeData: QRCode = new QRCode();
    qrcodeData.cooperative = this.params.coop;
    qrcodeData.day = this.params.day;
    qrcodeData.hour = this.params.hour;
    qrcodeData.trajet = this.params.trajet$;
    qrcodeData.class = this.params.class;
    qrcodeData.uid = this.params.uid;
    qrcodeData.reservation = String(reservation$);
    this.qrcodeProvider.save(qrcodeData,String(this.qrCode.qrcode_id));
  }

  add_sale(){
   let sales = new Sale({
      qrReservation: String(this.sale.sale_qrReservation),
      benefit: this.sale.sale_benefit,
      status: this.sale.sale_status
    });
    this.saleProvider.customPath(`${this.sale.sale_path}`);
    this.saleProvider.save(sales, String(this.sale.sale_qrReservation));
            
  }

}