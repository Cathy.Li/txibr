import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { BrokerMenuPage } from './broker-menu';

@NgModule({
  declarations: [
    BrokerMenuPage,
  ],
  imports: [
    IonicPageModule.forChild(BrokerMenuPage),
  ],
})
export class BrokerMenuPageModule {}
