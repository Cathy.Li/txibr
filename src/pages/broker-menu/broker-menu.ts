import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { BrokerRegisterPage } from '../../pages/broker-register/broker-register';
import { CooperativeBrokerPage } from '../../pages/cooperative-broker/cooperative-broker'


/**
 * Generated class for the BrokerMenuPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-broker-menu',
  templateUrl: 'broker-menu.html',
})
export class BrokerMenuPage {

  customer$: string;
  constructor(
  	public navCtrl: NavController, 
  	public navParams: NavParams
  	) {
  	this.customer$ = this.navParams.get('customer$');
    // this.customer$ = "2241575522759920";
  }

  ionViewDidLoad() {
    
  }

  goToBroker(){
  	this.navCtrl.push(BrokerRegisterPage,{customer$: this.customer$});
  }

  goToCooperativeBroker(){
  	this.navCtrl.push(CooperativeBrokerPage, {customer$: this.customer$})
  }

}
