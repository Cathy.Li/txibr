import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { Reservation } from '../../models/reservation.model';
import { ReservationProvider } from '../../providers/reservation/reservation';
import { BookingDetailsPage } from '../booking-details/booking-details';
import { StatusPaiementPage } from '../status-paiement/status-paiement';


/**
 * Generated class for the QrpaiementPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

 @IonicPage()
 @Component({
 	selector: 'page-finpay-paiement',
 	templateUrl: 'finpay-paiement.html',
 })
 export class FinpayPaiementPage {
 	form: FormGroup;
 	card: string;
 	booking$: string;
 	reservation$: string;
 	reservation: Reservation = new Reservation();
 	params: any;

 	constructor(
 		public navCtrl: NavController, 
 		public navParams: NavParams, 
 		private formBuilder: FormBuilder ,
 		private reservationProvider: ReservationProvider
 	) {
 		this.form = this.formBuilder.group({
 			card: ['',Validators.required],
 			expiration: ['',Validators.required],
 			pin: ['',Validators.required]
 		});
 		this.init()
 	}

 	init(){

 		this.params = this.navParams.get('params');
 		this.card = this.navParams.get('result');

 		
 		let path = `${this.params.path}/${this.params.class}/reservation/${this.params.uid}`;

 		this.reservationProvider.customPath(path);

 		this.reservationProvider.fetch(this.params.reservation$).then((reservation: Reservation)=>{
 			this.reservation = reservation;
 		})


 	}

 	ionViewDidLoad() {
 		

 	}

 	processPaiement(){
 		this.reservation.status = 'completed';

 		//this.reservationProvider.save(this.reservation,this.params.reservation$);

 		//this.navCtrl.setRoot(BookingDetailsPage, {params: this.params});

		//Jean soa
 		let id_save = this.reservationProvider.save(this.reservation,this.params.reservation$);
 		
 		this.navCtrl.push(StatusPaiementPage, {params: this.params,id : id_save});
 	}

 }
