import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { FinpayPaiementPage } from './finpay-paiement';

@NgModule({
  declarations: [
    FinpayPaiementPage,
  ],
  imports: [
    IonicPageModule.forChild(FinpayPaiementPage),
  ],
})
export class FinpayPaiementPageModule {}
