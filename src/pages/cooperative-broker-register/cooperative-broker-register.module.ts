import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CooperativeBrokerRegisterPage } from './cooperative-broker-register';

@NgModule({
  declarations: [
    CooperativeBrokerRegisterPage,
  ],
  imports: [
    IonicPageModule.forChild(CooperativeBrokerRegisterPage),
  ],
})
export class CooperativeBrokerRegisterPageModule {}
