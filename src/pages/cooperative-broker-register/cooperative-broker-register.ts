import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { BrokerProvider } from '../../providers/broker/broker';
import { Broker } from '../../models/broker.model';
import { PopUpProvider } from '../../providers/pop-up/pop-up';
import { BrokerMenuPage } from '../../pages/broker-menu/broker-menu';
import { BrokerRequestProvider } from '../../providers/broker-request/broker-request';
import { BroKerRequest } from '../../models/broker-request.model';

/**
 * Generated class for the CooperativeBrokerRegisterPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-cooperative-broker-register',
  templateUrl: 'cooperative-broker-register.html',
})
export class CooperativeBrokerRegisterPage {

 	form: FormGroup; 
  	customer$: string;
  	customer_name$: string;
  	cooperative$: any;
  	constructor(public navCtrl: NavController,
	   	public navParams: NavParams,
	  	private formBuilder: FormBuilder,
	  	private brokerProvider: BrokerProvider,
	  	private popupProvider: PopUpProvider,
  		private requestProvider: BrokerRequestProvider
	  	) {
  		this.form = this.formBuilder.group({
 			name: ['',Validators.required],
 			address: ['',Validators.required],
 			contact: ['',Validators.required]
 		});
  		this.customer$ = this.navParams.get('customer$');
  		//this.customer_name$ = this.navParams.get('customer_name$');
  		this.cooperative$ = this.navParams.get('params');
  }

  onSubmit(){
  	if (this.form.valid) {
  		this.popupProvider.presentConfirm('Voulez-vous envoyer la formulaire de démande pour devenir agent de ' + this.cooperative$.name + '?').then((ok)=>{
  			this.form.value.status = false;
  			this.requestProvider.customPath(`cooperative/${this.cooperative$.key}/broker_request`);
      		this.requestProvider.save(new BroKerRequest(this.form.value), this.customer$ );
        	this.navCtrl.push(BrokerMenuPage,{customer$: this.customer$});
  		}, (cancel)=>{})
  	}
  }
}
