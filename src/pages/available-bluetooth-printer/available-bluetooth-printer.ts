import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { PrinterProvider } from '../../providers/printer/printer';


@IonicPage()
@Component({
	selector: 'page-available-bluetooth-printer',
	templateUrl: 'available-bluetooth-printer.html',
})
export class AvailableBluetoothPrinterPage {

	content: any;
	printers = [];
	qrcodeHex: any;
	spiner = true;
	constructor(
		public navCtrl: NavController, 
		public navParams: NavParams,
		private printerProvider: PrinterProvider
		) {
		// this.content = this.navParams.get('content');

		// this.init()
	}

	ngOnInit(){
		this.content = this.navParams.get('content');
	    this.init();
	}

	init(){
		this.printerProvider.findAllBluetoothPrinters().then((devices: Array <Object>)=>{
			this.printers = [];
			this.printers = devices;
			this.spiner = false;
		},(fail) => {
			this.spiner = false;
		});
	}

	ionViewDidLoad() {

	}

	refresh(refresher){
		this.printerProvider.refreshBluetoothPrintersList(refresher).then((devices: Array <Object>)=>{
			this.printers = [];
			this.printers = devices;
		});
	}

	processPrintBT(printer){
		this.printerProvider.writeDataToBluetoothSerial(printer,this.content);
	}

}
