import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AvailableBluetoothPrinterPage } from './available-bluetooth-printer';

@NgModule({
  declarations: [
    AvailableBluetoothPrinterPage,
  ],
  imports: [
    IonicPageModule.forChild(AvailableBluetoothPrinterPage),
  ],
})
export class AvailableBluetoothPrinterPageModule {}
