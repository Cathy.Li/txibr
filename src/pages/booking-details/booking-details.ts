import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController, AlertController, Loading , LoadingController  } from 'ionic-angular';
import { CarProvider } from '../../providers/car/car';
import { ReservationProvider } from '../../providers/reservation/reservation';
import { CooperativeProvider } from '../../providers/cooperative/cooperative'
import { Car } from '../../models/car.model';
import { Reservation } from '../../models/reservation.model';
import { PaiementMethodPage } from '../paiement-method/paiement-method';
import { PrinterWidgetPage } from '../printer-widget/printer-widget';
import { BookingClassProvider } from '../../providers/booking-class/booking-class';
import { EventProvider } from '../../providers/event/event';
// import * as EscPosEncoder  from 'esc-pos-encoder';
import { MapPage } from '../map/map';
import { Coordinate } from '../../models/coordinate.model';

import { BrokerProvider } from '../../providers/broker/broker';
import { Broker } from '../../models/broker.model';
import { BrokerRequestProvider } from '../../providers/broker-request/broker-request';


import { BillingProvider } from '../../providers/billing/billing';
import { QrcodeProvider } from '../../providers/qrcode/qrcode';
import { QRCode } from '../../models/qrcode.model';
import { PlaceProvider } from '../../providers/place/place';
import { Place } from '../../models/place.model';
import { SaleProvider } from '../../providers/sale/sale';
import { UtilProvider } from '../../providers/util/util';


import { AvailableBookingPage } from '../available-booking/available-booking';
import { MyBookingsPage } from '../my-bookings/my-bookings';

import { CyclosProvider } from '../../providers/cyclos/cyclos';
import { AccountProvider } from '../../providers/cyclos/account';
import { Credential } from '../../lib/credential';
import { Payment, PaymentType, CustomValues } from '../../lib/models/payment';
import { PaymentProvider } from '../../providers/cyclos/payment';
import { Config } from '../../config/config'
import { OrchidProvider } from '../../providers/orchid/orchid';
import { UserProvider } from '../../providers/cyclos/user';
import { AuthProvider } from '../../providers/cyclos/auth';
import { Transfer } from '../../lib/models/transfer';
import { Session } from '../../lib/models/session';
import { StorageProvider } from '../../providers/storage/storage';
import { Storage } from '@ionic/storage';
import { PopUpProvider } from '../../providers/pop-up/pop-up';
import * as moment from "moment"; 

@IonicPage()
@Component({
	selector: 'page-booking-details',
	templateUrl: 'booking-details.html',
})
export class BookingDetailsPage {

	booking$: string;
	reservation$: string;
	paid: boolean;
	cooperative$: string;
	car$: string;
	details = {} ;
	qrcode: string;
	params: any;
	print: boolean = false;
	canvas: any;
	qrcodeHex: any;
	data:any;
	bytes: any;
	broker: any;
	amount_trans: number;
	request = {} ;

  	session : Session;
  	loading: Loading;
  	credential:Credential;
   	user:any = {
     	length: false
   	};
   	customValues : CustomValues;
  	IdOrchiDest  : string;
  	idTransaction: string;
  	users :string;

	constructor(
		private navCtrl: NavController,
		private navParams: NavParams,
		private carProvider: CarProvider,
		private reservationProvider: ReservationProvider,
		private cooperativeProvider: CooperativeProvider,
		private modalCtrl: ModalController,
		private bookingclassProvider: BookingClassProvider,
		private eventProvider: EventProvider,
    	private brokerProvider: BrokerProvider,
    	private requestProvider: BrokerRequestProvider,
	    public billingProvider: BillingProvider,
      	private alertCtrl: AlertController,
	    public qrcodeProvider: QrcodeProvider,
	    public placeProvider: PlaceProvider,
	    public saleProvider: SaleProvider,
	    public utilProvider: UtilProvider,
	    private accountPrvd: AccountProvider,
	    private paymentPrvd: PaymentProvider, 
	    private loadingCtrl: LoadingController,
	    private orchidPrvd: OrchidProvider,
	    public accountPrd: AccountProvider,
	    private userPrvd: UserProvider,
	    public auth: AuthProvider,
    	private storage: Storage,
      	private storagePrvd:StorageProvider,
    	private popupProvider: PopUpProvider
		){	
		// this.init();
	}

	 ngOnInit(){
	    this.init();
	}

	init(){
		this.params = this.navParams.get('params');
		this.eventProvider.setEvent('customer$',{customer$: this.params.uid});
		/*this.bookingclassProvider.customPath(`cooperative/${this.params.coop}/booking_class`);
		this.bookingclassProvider.fetch(this.params.class).then((bookingclass)=>{
			this.details['class'] = bookingclass;*/
			this.details['reference'] = this.params.reservation$;
			this.details['trajet'] = this.params.trajet;
			this.details['date'] = this.params.date;
			this.details['hour'] = this.params.hour;
			this.cooperativeProvider.fetch(this.params.coop).then((cooperative)=>{
				this.details['cooperative'] = cooperative;

				let rpath = `${this.params.path}/${this.params.class}/reservation/${this.params.uid}`;
				this.reservationProvider.customPath(rpath);
				this.reservationProvider.fetch(this.params.reservation$).then((reservation: Reservation)=>{
					this.details['reservation'] = reservation;

					this.paid = false;

					if (reservation.status == 'completed') {
						this.paid = true;
					}

					this.qrcode = reservation.qrcode;

					this.carProvider.customPath(`cooperative/${this.params.coop}/car`);
					this.carProvider.fetch(this.params.matricule).then((car: Car)=>{
						this.details['car'] = car;
					});

					this.brokerProvider.fetch(this.params.uid).then((broker: Broker)=>{ 
				      	if (broker) {
					        //broker for all
					        this.broker = broker;
							this.billingProvider.customPath(`billing/${this.params.uid}`);
						    this.billingProvider.fetch(this.details['reservation'].billing).then((billing)=>{
						       	if (billing) {
						        	this.details['client'] = billing;
						        }
						    });
				      	}else{
					        //broker for cooperative
					        this.requestProvider.customPath(`cooperative/${this.params.coop}/broker_request`);
					        this.requestProvider.fetch(this.params.uid).then((broker: Broker)=>{
					          if (broker) {
					            // code...
					            this.broker = broker;
								this.billingProvider.customPath(`billing/${this.params.uid}`);
							    this.billingProvider.fetch(this.details['reservation'].billing).then((billing)=>{
							       	if (billing) {
							        	this.details['client'] = billing;
							        }
							    });
					          }
					        })
				      	}
				    })
				})
			});			
		// })

	}



	processPaiement(){
		this.navCtrl.push(PaiementMethodPage, {params: this.params});
	}

	editBooking(){
		this.request['startlocation'] = this.params['trajet']['startstation'].city;
		this.request['destination'] = this.params['trajet']['arrivalstation'].city;
		this.request['date'] = this.params['date'];
		this.request['hour'] = this.params['hour'];
		this.request['day'] = this.params['day'];
		this.request['amount'] = this.details['reservation'].amount;
		this.request['rest'] = this.details['reservation'].rest;
		this.request['class'] = this.params['class'];
		this.request['cooperative'] = this.params['coop'];
		this.request['reservation$'] = this.params['reservation$'];
		this.request['matricule'] = this.params['matricule'];
		this.request['path'] = this.params['path'];
		this.request['uid'] = this.params['uid'];
        this.navCtrl.push(AvailableBookingPage, {request: this.request, customer$: this.params['uid']});
	}

	cancelBooking(){
		const prompt = this.alertCtrl.create({
	      title: "Confirmation",
	 
	      message: "Voulez-vous annuler votre réservation ?\n\n\n\nEn cas d'annulation, aucun remboursement ne sera accordé.\n\n\n\nSi Oui,annuler ce réservation ",
	      buttons: [
	        {
	          text: 'Non',
	          handler: data => {
	            console.log('Cancel clicked');
	          }
	        },
	        {
	          text: 'Oui',
	          handler: data => {
	          	this.reservation_cancel();
	          }
	        }
	      ],
	        cssClass: 'tb-alert'
	    });
	    prompt.present();

	}

	showPrinterWidget(){

		if (this.details['reservation'].status == "pending") {
			status = "en attente de paiement"
		}
		else{
			status = "payé";
		}

		const date_depart = moment(this.details['date'], 'DD/MM/YYYY') ;

		const nom_agent = {
			label:'AGENT DE RESERVATION : ',
			value:this.details['client'].name
		}
		const nom_client = {
			label:'NOM CLIENT : ',
			value:this.details['client'].name_reserver
		}
		const cin_client = {
			label:'N° CIN CLIENT : ',
			value:this.details['client'].cin_reserver
		}
		const rest_payer = {
			label:'RESTE  : ',
			value:this.details['reservation'].rest+' Ar'
		}


		let content = [
		{
			label: '*',
			value: 'Ticket de reservation'
		},
		{
			label: 'Cooperative : ',
			value: this.details['cooperative'].name
		},
		nom_agent,
		this.details['client'].name_reserver? nom_client:'',
			this.details['client'].cin_reserver? cin_client:'',
		{
			label: 'Ville de depart : ',
			value: this.details['trajet'].startstation.city
		},
		{
			label: 'Gare de depart : ',
			value: this.details['trajet'].startstation.name
		},
		{
			label: 'Ville destination : ',
			value: this.details['trajet'].arrivalstation.city
		},
		{
			label: 'Gare destionation : ',
			value: this.details['trajet'].arrivalstation.name
		},
		{
			label: 'Date de voyage : ',
			value: date_depart+" à "+this.details['hour']
		},
		{
			label: 'Voiture N° : ',
			value: this.details['car'].matricule
		},
		{
			label: 'Nombre de place : ',
			value: this.details['reservation'].nbplace
		},
		{
			label: 'Montant : ',
			value: this.details['reservation'].amount +' Ar'
		},
		this.details['reservation'].rest > 0 ? rest_payer : '',
		{
			label: 'Statut de paiement : ',
			value: status
		},

		{
			label: 'qrcode : ',
			value: this.qrcode
		}

		]


		//let details = <HTMLElement> document.querySelector('#details');
		let details = document.querySelector('#details');

		let modal = this.modalCtrl.create(PrinterWidgetPage, {htmlContent:details, content:content}, {cssClass:'pricebreakup' });

		modal.present();

		modal.onDidDismiss(()=>{
			this.print = false;
		})
	}

	showMap(){

		var origin = new Coordinate({
			lat: this.details['trajet'].startstation.latitude,
			lng: this.details['trajet'].startstation.longitude
		});

		var destination = new Coordinate({
			lat: this.details['trajet'].arrivalstation.latitude,
			lng: this.details['trajet'].arrivalstation.longitude
		});

		var route = {
			origin: origin,
			destination: destination
		}

		this.navCtrl.push(MapPage, route);
	}

	reservation_cancel(){

    	let pm = this.utilProvider.purcentageAgent(this.details['reservation'].amount,this.details['cooperative'].penaliteCanc);
	    // vola transfert = reste à payer + penalité - montant
	    this.amount_trans = this.details['reservation'].rest+pm-this.details['reservation'].amount;

	    console.log(this.amount_trans);
	    //this.verifie();
	    // this.verifie()
	    
	    	let path = `${this.params.path}/${this.params.class}/reservation/${this.params.uid}`;
		    this.reservationProvider.customPath(path);
		    this.reservationProvider.fetch(this.params.reservation$).then((reservation: Reservation)=>{
		      let place_list = [];
		      if (reservation.placelist) {        
		          place_list = JSON.parse(reservation.placelist)
		      }
		      //delete billing
		      this.billingProvider.customPath(`billing/${this.params.uid}`);
		      this.billingProvider.remove(reservation.billing);

		      //delete qrcode
		      this.qrcodeProvider.fetcAll().subscribe((qrcode: QRCode)=>{
		        if (qrcode) {
		          for(let qr in qrcode){
		            if (qrcode[qr].cooperative === this.params.coop && qrcode[qr].reservation === this.params.reservation$ && qrcode[qr].uid === this.params.uid) {
		              this.qrcodeProvider.remove(qr);
		              this.delete_vente(qr);
		            }
		          }
		        }
		      });

		      //remove_place_reserved
		      this.remove_place_reserved(place_list);

		      // delete reservation
		      this.reservationProvider.customPath(path);
		      this.reservationProvider.delete(this.params.reservation$);
		      this.popupProvider.PresentToast('votre réservation est annulé  avec succès!',true);
	          this.navCtrl.setRoot(MyBookingsPage ,{ customer$: this.params.uid });
		    });
	    
 	
	}


	remove_place_reserved(place_list){
	      let _occupedd = [];
	      let path_car = `cooperative/${this.params.coop}/car/${this.params.matricule}/place_reserved/${this.params.date}`;
	      this.placeProvider.customPath(path_car);
	      this.placeProvider.fetch(this.params.hour).then((place: Place)=>{

	        if (place && place.reserved) {        
	          _occupedd = JSON.parse(place.reserved);
	          for( let _place_list in place_list) {
	            _occupedd.splice(place_list[_place_list],1);
	          }
	        }
	        let _pace = new Place();
	        
	        _pace.reserved = JSON.stringify(_occupedd);
	        this.placeProvider.customPath(path_car);
	        this.placeProvider.save(_pace,this.params.hour);
	      })
	  }

   public delete_vente(qrcode: string ) {
      //delete vente
      //is brooker
      this.brokerProvider.fetch(this.params.uid).then((broker: Broker)=>{ 
        if (broker) {
            this.saleProvider.customPath(`broker/${this.params.uid}/sale`);
            this.saleProvider.remove(qrcode);
        }
        else{
          //broker for cooperative
          this.requestProvider.customPath(`cooperative/${this.params.coop}/broker_request`);
          this.requestProvider.fetch(this.params.uid).then((broker: Broker)=>{
            if (broker) {
              this.saleProvider.customPath(`cooperative/${this.params.coop}/broker_request/${this.params.uid}/sale`);
              this.saleProvider.remove(qrcode);
            }
          })
        }
      })
  	}


    verifie() {
    const prompt = this.alertCtrl.create({
      title: "",
 
      message: "Entrer login et mot de passe pour confirmer cet annulation !",
      inputs: [
      	{
          name: "username",
          placeholder: 'Identifiant',
          type : "text",
        },
        {
          name: "password",
          placeholder: 'Mot de passe',
          type : "password",
        },
      ],
      buttons: [
        {
          text: 'Annuler',
          handler: data => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'OK',
          handler: data => {
            this.user.principal = data.username;
            this.user.password = data.password;
            this.Login();
          }
        }
      ],
        cssClass: 'tb-alert'
    });
    prompt.present();
  }

  protected Login() {
  	this.presentLoading();
  	this.credential = new Credential({
      name: this.user.principal,
      password: this.user.password
    })
  	this.auth.login(this.credential).then((session: Session) => {
        CyclosProvider.session = session;
        this.session = CyclosProvider.session;
        /* validation account orchid-cyclos*/ 
        this.auth.isExistIdOrhid(this.credential).then((customValues :CustomValues) => {

          this.setStorage(session)
          this.Verification(session);
          //this.alert("Compte déja validé")
        }).catch((customValues) => {
          this.orchidPrvd.isExistAccount(customValues.cin).then((account:any) => {
          	console.log("account 1 ::::"+customValues)
              if(account.status) {
                this.auth.validateAccount(this.credential, account.idOrchid).then((success) => {
                  this.alert("Votre compte a bien été validé")
                  console.log("success 1 ::::"+customValues)
                  this.setStorage(session)
                  this.Verification(session);
                }).catch(error => {
                  this.alert("Erreur lors de la validation de votre compte")
                  this.loading.dismiss();
                })
              }else {
                this.alert("Votre compte est en cours de validation")
                this.loading.dismiss();
              }
            }).catch((error) => {
              this.alert("vous n'avez pas encore de compte créez-en un")
              this.loading.dismiss();
          })
        })
        /* validation account orchid-cyclos */ 
      }).catch((error) => {
        this.loading.dismiss();
      })
    
  }

  Verification(session){
    console.log(session)
    this.auth.isExistIdOrhid(this.credential).then((customValues :CustomValues) =>{
      this.customValues = customValues;
      this.IdOrchiDest =  this.customValues['orchid_account'];
       console.log(this.customValues['orchid_account']);
    })

    if(this.details['cooperative'].telContact){
	    this.userPrvd.getUsersbycontact(session,this.details['cooperative'].telContact).then((user : any) => {   
	        console.log(user);
	        if (user.length >0) {
	           	this.user = user;
	            this.user.length = user.length;
	            this.users = this.user;
	        }
	           
	    })
    }

    this.accountPrvd.getAccountList(this.credential)
    .then((acccount) => {
    this.ConfirmTransfer()
   
    }, (error) => {
    	this.loading.dismiss();
    	this.alert("Mots de passe invalide","Attention");
    })
  }

  ConfirmTransfer() { 
    this.presentLoading();
    this.amount_trans = 9;
    let transaction: Payment = {
      amount: this.amount_trans,
      description: 'de',
      subject: this.user[0].id,
      type: PaymentType.USER_TRADE
    }
    //let test = this.IdOrchiDest
    //console.log(test)
    let transfer: Transfer = {
      creditedAccountId: this.user[0].customValues.orchid_account,
      debitedAccountId:this.IdOrchiDest ,
      amount: this.amount_trans,
      wording: "Frais de transport "
    }
    console.log(transfer)
 
    console.log(transaction)
      
      this.storagePrvd.getSession().then((session : Session) => {
        console.log(session.user.id);
        this.paymentPrvd.setTransaction( transaction, this.session).then((payment :any ) => {
            console.log(payment);
          this.idTransaction = payment.id;
          let idorchidPrvd = this.orchidPrvd.setTransfer(transfer);

    	console.log(idorchidPrvd)

          this.loading.dismiss();
        }).catch((error) => {
          this.loading.dismiss();
          console.log(error)
        })
      }).catch((error) => {
        console.log(error)
        this.loading.dismiss()
      }) 
  }

  	protected presentLoading() {
	    this.loading =  this.loadingCtrl.create()
	    return this.loading.present()
	}

	private setStorage(session) {
	    this.storage.set(Config.SESSION_STO, session).then(() => {
	      console.log(session);
	    })
	}

	private alert(message:any,title?:any) {
		const prompt = this.alertCtrl.create({
	      	title: title,
	      	message: message,
	      	buttons: [{
	        	text: 'OK'
	        }],
        	cssClass: 'tb-alert'
	    });
	    prompt.present();
	}

}
