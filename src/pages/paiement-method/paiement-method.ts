import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';
import { QrScannerPage } from '../qr-scanner/qr-scanner'


import { LoginFinpayPage } from '../login-finpay/login-finpay'

@IonicPage()
@Component({
	selector: 'page-paiement-method',
	templateUrl: 'paiement-method.html',
})
export class PaiementMethodPage {

	booking$: string;
	reservation$: string;
	params: any;
	constructor(
		public navCtrl: NavController, 
		public navParams: NavParams,
    	public alertCtrl: AlertController
		) {
		// this.init()

	}

	ngOnInit(){
	    this.init();
	}

	init(){
		this.booking$ = this.navParams.get('booking$');
		this.reservation$ = this.navParams.get('reservation$');
		this.params = this.navParams.get('params');

	}

	ionViewDidLoad() {

	}

	processQRCScanner(){
		this.navCtrl.push(QrScannerPage, { params: this.params});		
	}

	processfinpay(){
		this.navCtrl.push(LoginFinpayPage, { params: this.params});		
	}
	
	mobileMoney(){
		//this.navCtrl.push(LoginFinpayPage);	
        this.alert("Ce mode de paiement sera disponible bientôt");	
	}

	private alert(message:any,title?:any) {
    this.alertCtrl.create({
      title: title,
      message: message,
      buttons: [{
        text: 'OK'
        }]
    }).present()
  }

}
