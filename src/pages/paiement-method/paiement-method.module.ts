import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PaiementMethodPage } from './paiement-method';

@NgModule({
  declarations: [
    PaiementMethodPage,
  ],
  imports: [
    IonicPageModule.forChild(PaiementMethodPage),
  ],
})
export class PaiementMethodPageModule {}
