import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { FacebookProvider } from '../../providers/facebook/facebook';
import { QuickBookingPage } from '../quick-booking/quick-booking';
import { MyBookingsPage } from '../my-bookings/my-bookings';
import { LoginPage } from '../login/login';
import { EventProvider } from '../../providers/event/event'
import { MailEncryptProvider } from '../../providers/mail-encrypt/mail-encrypt';


import { BrokerRequestProvider } from '../../providers/broker-request/broker-request';
import { CooperativeProvider } from '../../providers/cooperative/cooperative';
import { BrokerProvider } from '../../providers/broker/broker';
import { Broker } from '../../models/broker.model';

/**
 * Generated class for the ConnectedPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-connected',
  templateUrl: 'connected.html',
})
export class ConnectedPage {

  user: any;
  customer$: string;
  isbroker: any;

  constructor(
  	public navCtrl: NavController,
  	public navParams: NavParams,
  	private facebookProvider: FacebookProvider,
    private eventProvider: EventProvider,
    public mailEncryptProvider: MailEncryptProvider,
    private brokerProvider: BrokerProvider,
    private requestProvider: BrokerRequestProvider,
    private cooperativeProvider: CooperativeProvider) {

  	// this.init();

  }

  
  ngOnInit(){
      this.init();
  }

  init(){
  	this.facebookProvider.getUser().then((profile)=>{
  		this.user = profile;
      this.customer$ = this.user['id'];
      this.eventProvider.setEvent('customer$',{customer$: this.customer$});

        this.brokerProvider.fetch(this.user['id']).then((broker: Broker)=>{
          
          if (broker) {
            //if (broker.status == false) {
              this.isbroker = broker;
            //}
          }else{
            this.cooperativeProvider.fetcAll().subscribe((cooperatives)=>{
              for(let key in cooperatives){
                this.requestProvider.customPath(`cooperative/${key}/broker_request`);
                this.requestProvider.fetch(this.user['id']).then((broker: Broker)=>{
                  if (broker) {
                    this.isbroker = broker;
                  }
                })
              }
            })
          }
        })
  	});
  }

  ionViewDidLoad() {

  }

  startBooking(){
  	this.navCtrl.push(QuickBookingPage, {customer$:this.customer$})
  }

  myBookings(){
  	this.navCtrl.push(MyBookingsPage, {customer$:this.customer$});
  }


  logout(){
  	this.facebookProvider.logout().then((ok)=>{
      if (ok) {
        this.navCtrl.setRoot(LoginPage);
      }
    })
  }

}
