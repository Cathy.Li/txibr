import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SeatPickerPage } from './seat-picker';

@NgModule({
  declarations: [
    SeatPickerPage,
  ],
  imports: [
    IonicPageModule.forChild(SeatPickerPage),
  ],
})
export class SeatPickerPageModule {}
