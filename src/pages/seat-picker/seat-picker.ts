import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams , AlertController } from 'ionic-angular';
import { CarProvider } from '../../providers/car/car';
import { Car } from '../../models/car.model';
import { Voyage } from '../../models/voyage.model';
import { CarTypeProvider } from '../../providers/car-type/car-type';
import { CarType } from '../../models/car-type.model';
import { Reservation } from '../../models/reservation.model';
import { FacebookProvider } from '../../providers/facebook/facebook';
import { Profile } from '../../models/profile.model';
import { ReservationProvider } from '../../providers/reservation/reservation';
import { BookingDetailsPage } from '../booking-details/booking-details';
import { PlaceProvider } from '../../providers/place/place';
import { Place } from '../../models/place.model';
import { Billing } from '../../models/billing.model';
import { BillingProvider } from '../../providers/billing/billing';
import { Md5 } from 'ts-md5/dist/md5';
import { QrcodeProvider } from '../../providers/qrcode/qrcode';
import { QRCode } from '../../models/qrcode.model';
import { BrokerProvider } from '../../providers/broker/broker';
import { Broker } from '../../models/broker.model';
import { Sale } from '../../models/sale.model';
import { SaleProvider } from '../../providers/sale/sale';
import { BrokerRequestProvider } from '../../providers/broker-request/broker-request';
import { UtilProvider } from '../../providers/util/util';
import { CooperativeProvider } from '../../providers/cooperative/cooperative';
import { PlanningProvider } from '../../providers/planning/planning';


import { PaiementMethodPage } from '../paiement-method/paiement-method';
import { PopUpProvider } from '../../providers/pop-up/pop-up';


@IonicPage()
@Component({
  selector: 'page-seat-picker',
  templateUrl: 'seat-picker.html',
})
export class SeatPickerPage {

  class: string;
  matricule: string;
  nbRows: number;
  nbCols: number;
  rows: string[] = [];
  cols: number[]  = [];
  alpha = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
  notavailable: string[] = [];
  reserved: string[] = [];
  selected: string[] = [];
  amount: number;
  voyage: Voyage = new Voyage();
  car: Car = new Car();
  coop: string;
  price: any;
  path: string;
  params: any;
  billing: Billing;
  user: Profile = new Profile();

  broker: any;
  cooperative: any;
  request: any;
  rest: number;
  rester: number;
  days: number;
  sale_status: boolean;

  reservation_status: string;

  brockerCoop: boolean;
  brockerAll: boolean;
  reservation: Reservation = new Reservation();

  my_reserved: string[] = [];

  constructor(
    private navCtrl: NavController,
    private navParams: NavParams,
    private carProvider: CarProvider,
    private cartypeProvider: CarTypeProvider,
    private facebookProvider: FacebookProvider,
    private reservationProvider: ReservationProvider,
    private placeProvider: PlaceProvider,
    private planningProvider: PlanningProvider,
    private billingProvider: BillingProvider,
    private qrcodeProvider: QrcodeProvider,
    private brokerProvider: BrokerProvider,
    private saleProvider: SaleProvider,
    private requestProvider: BrokerRequestProvider,
    private utilProvider: UtilProvider,
    private cooperativeProvider: CooperativeProvider,
    private alertCtrl: AlertController,
    private popupProvider: PopUpProvider
    
    ){  

    this.request = this.navParams.get('request');
    
    this.billing = new Billing();
    this.facebookProvider.getUser().then((user: Profile)=>{
      this.user = user;
      this.billing.name = user.name;
    });/*
    this.user.id = "2241575522759918";
    this.billing.name = "Mananur Jean";*/
    // this.init();
  }

  
  ionViewDidEnter() {
    // this.init();
    this.amount = this.selected.length * this.price;
  }

  ngOnInit(){
    this.init();
  }


  
  init(){
    this.params = this.navParams.get('params')

    if (this.request.reservation$) {
      this.days = this.utilProvider.differenceOfDate(this.request.date,this.params.date);
    }

    this.class = this.params.class
    this.matricule = this.params.matricule;
    this.coop = this.params.coop;
    this.price = this.params.price;
    this.path = this.params.path;

    let cpath = `cooperative/${this.coop}/car`;

    this.carProvider.customPath(cpath);
    this.carProvider.fetchOne(this.matricule).subscribe((car: Car)=>{

      this.car = car;

      this.notavailable = [];
      this.selected = [];
      this.reserved = [];

      let ppath = `${cpath}/${this.matricule}/place_reserved/${this.params.date}`;
      this.placeProvider.customPath(ppath);
      this.placeProvider.fetchOne(this.params.hour).subscribe((place: Place)=>{

        if (place && place.reserved) {
          let occuped = [];
          occuped = JSON.parse(place.reserved);
          for(let o in occuped){
            if (this.request.reservation$) {
               this.seatstatus(occuped[o]);
            }else{
              this.reserved.push(occuped[o]);
            } 
            
          }

        }
        

      })


      this.cartypeProvider.fetch(car.cartype).then((cartype: CarType)=>{

        this.amount = 0;
        this.nbCols = cartype.nbCols;
        this.nbRows = cartype.nbRows;
        this.notavailable = JSON.parse(cartype.notavailable);
        this.rows = [];
        this.cols = [];
        let refRows = this.alpha.split('');      

        for( let i = 0; i < this.nbRows; i++){
          this.rows.push(refRows[i]);               
        }

        for( let j = 0; j < this.nbCols; j++){
          this.cols.push(j);
        }


      });


    })

    this.cooperativeProvider.fetch(this.coop).then((cooperative)=>{
      this.cooperative = cooperative;
    });

    this.brokerProvider.fetch(this.user.id).then((broker: Broker)=>{ 
      if (broker) {
        //broker for all
        this.brockerAll = true;
        this.broker = broker;
      }else{
        //broker for cooperative
        this.requestProvider.customPath(`cooperative/${this.coop}/broker_request`);
        this.requestProvider.fetch(this.user.id).then((broker: Broker)=>{
          if (broker) {
            // code...
            this.brockerCoop = true;
            this.broker = broker;
          }
        })
      }
    })

  }


  getStatus = function(seatPos: string) {

    

    if( this.notavailable.indexOf(seatPos) !== -1 ){
      return 'notavailable';
    }

    if(this.reserved.indexOf(seatPos) !== -1) {
      return 'reserved';
    } else if(this.selected.indexOf(seatPos) !== -1) {
      return 'selected';
    }
  }

  clearSelected = function() {
    this.selected = [];
    this.amount = 0;
  }

  seatClicked = function(seatPos: string) {

    var index = this.selected.indexOf(seatPos);

    if(index !== -1) {
      this.selected.splice(index, 1)
    } else {
      if((this.reserved.indexOf(seatPos) === -1) && (this.notavailable.indexOf(seatPos) === -1))
          this.selected.push(seatPos);
    }

    this.amount = this.selected.length * this.price;
  }


  saveSelectedSeat(){ 
  //is reservation
  
  if (this.request.reservation$) {
    let path = `${this.request.path}/${this.request.class}/reservation/${this.request.uid}`;
    this.reservationProvider.customPath(path);
    this.reservationProvider.fetch(this.request.reservation$).then((reservation: Reservation)=>{
      let place_list = [];
      if (reservation.placelist) {        
          place_list = JSON.parse(reservation.placelist)
      }
      //delete billing
      this.billingProvider.customPath(`billing/${this.request.uid}`);
      this.billingProvider.remove(reservation.billing);

      //delete qrcode
      this.qrcodeProvider.fetcAll().subscribe((qrcode: QRCode)=>{
        if (qrcode) {
          for(let qr in qrcode){
            if (qrcode[qr].cooperative === this.request.cooperative && qrcode[qr].reservation === this.request.reservation$ && qrcode[qr].uid === this.request.uid) {
              this.qrcodeProvider.remove(qr);
              this.delete_vente(qr);
            }
          }
        }
      });

      //remove_place_reserved
      this.remove_place_reserved(place_list);

      // delete reservation
      this.reservationProvider.customPath(path);
      this.reservationProvider.delete(this.request.reservation$);
    });

    
    if(this.days>7){
      let pm = this.utilProvider.purcentageAgent(this.amount,this.cooperative.penaliteMod);
      this.rest = this.amount-this.request.amount+this.request.rest+pm; 
      this.amount= this.amount+pm;
    }else{
      this.rest = this.amount-this.request.amount+this.request.rest; 
      this.amount= this.amount;
    }

    if (this.rest>0) {
        this.reservation_status = 'pending';
        this.sale_status = false;
      }else{
        this.reservation_status = 'completed';
        this.sale_status = true;
      }

  /*else{
    this.rest = this.amount;
    this.reservation_status = 'pending';
    this.sale_status = false;
    if (this.brockerCoop) {
      let am = this.utilProvider.purcentageAgent(this.amount,this.cooperative.purcentBrokerC);
      this.rest = this.amount-am;
      this.amount = this.amount-am;
    }else if(this.brockerAll){
      let am = this.utilProvider.purcentageAgent(this.amount,this.cooperative.purcentBroker);
      this.rest = this.amount-am
      this.amount = this.amount-am;;
    }
  }*/

    let reservation = new Reservation();
    let qrcode = this.uniqid();
    reservation.nbplace = this.selected.length;
    reservation.placelist = JSON.stringify(this.selected);
    reservation.car = this.matricule;

    reservation.amount = this.amount;
    reservation.rest = this.rest;
    reservation.status = this.reservation_status;

  
    /*if (this.params.isCooperativeBroker) {
      reservation.status = 'completed';
    }*/
   
    
    reservation.date = this.params.date;
    reservation.qrcode = qrcode.toString();

    this.billingProvider.customPath(`billing/${this.user.id}`);
    let billing$ = String(this.billingProvider.save(this.billing));
    reservation.billing = billing$;
    reservation.customer = this.user.id;


    let rpath = `${this.path}/${this.class}/reservation/${reservation.customer}`;
    this.reservationProvider.customPath(rpath);
    let reservation$ = this.reservationProvider.save(reservation);

    let qrcodeData: QRCode = new QRCode();
    qrcodeData.cooperative = this.coop;
    qrcodeData.day = this.params.day;
    qrcodeData.hour = this.params.hour;
    qrcodeData.trajet = this.params.trajet$;
    qrcodeData.class = this.params.class;
    qrcodeData.uid = this.user.id;
    qrcodeData.reservation = String(reservation$);
    let hashKey = Md5.hashStr(qrcode.toString());
    this.qrcodeProvider.save(qrcodeData,String(hashKey));



    this.add_place_reserved();
    


    let params = this.params;
    params.reservation$ = reservation$;
    params.uid = reservation.customer;
    params.phone_coop = this.cooperative.telContact

    //is brooker
    this.brokerProvider.fetch(this.user.id).then((broker: Broker)=>{ 
      if (broker) {
        //if (broker.status == false) {
          let am = this.utilProvider.purcentageAgent(reservation.amount,this.cooperative.purcentBroker);

          let sale = new Sale({
            qrReservation: String(hashKey),
            benefit: am,
            status: this.sale_status
          });
          this.saleProvider.customPath(`broker/${this.user.id}/sale`);
          this.saleProvider.save(sale, String(hashKey));
        //}
      }
      else{
        //broker for cooperative
        this.requestProvider.customPath(`cooperative/${this.coop}/broker_request`);
        this.requestProvider.fetch(this.user.id).then((broker: Broker)=>{
          if (broker) {
            // code...
            let am = this.utilProvider.purcentageAgent(reservation.amount,this.cooperative.purcentBrokerC);

            let sale = new Sale({
              qrReservation: String(hashKey),
              benefit: am,
              status: this.sale_status
            });
            this.saleProvider.customPath(`cooperative/${this.coop}/broker_request/${this.user.id}/sale`);
            this.saleProvider.save(sale, String(hashKey));
            }
        })
      }
    })
    // this.navCtrl.push(BookingDetailsPage, {params: params});
    

    this.popupProvider.PresentToast('Changement a été modifié  avec succès!',true);
    this.navCtrl.push(BookingDetailsPage, {params: params});
    
    }else{

      let billings_amont = 0;

      let path_sale = null;

       let qrcode = this.uniqid();

      this.reservation_status = 'pending';
      this.sale_status = false;
      if (this.brockerCoop) {
        billings_amont = this.utilProvider.purcentageAgent(this.amount,this.cooperative.purcentBrokerC);
        path_sale = `cooperative/${this.coop}/broker_request/${this.user.id}/sale`;
      }else if(this.brockerAll){
        billings_amont = this.utilProvider.purcentageAgent(this.amount,this.cooperative.purcentBroker);
        path_sale= `broker/${this.user.id}/sale`;
      }


      let occupeed = [];
      let _place = new Place();
      let pl_path = `cooperative/${this.coop}/car/${this.matricule}/place_reserved/${this.params.date}`;
      this.placeProvider.customPath(pl_path);
      this.placeProvider.fetch(this.params.hour).then((place : Place)=>{
        if (place && place.reserved) {        
          occupeed = JSON.parse(place.reserved)
        }
        for( let key in this.selected) {
          occupeed.push(this.selected[key]);
        }
        _place.reserved = JSON.stringify(occupeed);     
      })

      let params = this.params;
      params.uid = this.user.id;
      params.phone_coop = this.cooperative.telContact;
      params['billing'] = {
        billing_broker: this.broker,
        billing_: billings_amont,
        billing_data: this.billing,
        billing_path: `billing/${this.user.id}`
      }
      params['reservation'] = {
        reservation_nbplace: this.selected.length,
        reservation_placelist: JSON.stringify(this.selected),
        reservation_car: this.matricule,
        reservation_amount: this.amount-billings_amont,
        reservation_status: this.reservation_status,
        reservation_date: this.params.date,
        reservation_hour: this.params.hour,
        reservation_qrcode: qrcode.toString(),
        reservation_path: `${this.path}/${this.class}/reservation/${this.params.uid}`
      }
      params['qrcode'] = {
        qrcode_id: Md5.hashStr(qrcode.toString())
      }

      params['sale'] = {
        sale_qrReservation: String(Md5.hashStr(qrcode.toString())),
        sale_benefit: billings_amont,
        sale_status: this.sale_status, 
        sale_path: path_sale
      }

      params['reserver'] = {
        reserver_path: `cooperative/${this.coop}/car/${this.matricule}/place_reserved/${this.params.date}`,
        reserver_hour: this.params.hour,
        reserver_place: _place
      }



      this.navCtrl.push(PaiementMethodPage, {params: params});
    }
  }

  uniqid(){
      var alphanum = 'abcdefghijklmnopqrstuvwxyz1234567890';
      var id = '';

      for (var i = 0; i < 10; ++i) {
        var index = parseInt((Math.random() * alphanum.length) + '');
        id += alphanum[index]
      }

      return id.toUpperCase();
  }


  remove_place_reserved(place_list){
    
      let _occupedd = [];
      let path_car = `cooperative/${this.request.cooperative}/car/${this.request.matricule}/place_reserved/${this.request.date}`;
      this.placeProvider.customPath(path_car);
      this.placeProvider.fetch(this.request.hour).then((place: Place)=>{

        if (place && place.reserved) {        
          _occupedd = JSON.parse(place.reserved);
          for( let _place_list in place_list) {
            _occupedd.splice(place_list[_place_list],1);
          }
        }
        let _pace = new Place();
        
        _pace.reserved = JSON.stringify(_occupedd);
        this.placeProvider.customPath(path_car);
        this.placeProvider.save(_pace,this.request.hour);
      })
  }

  add_place_reserved(){
    let occupeed = [];
    let pl_path = `cooperative/${this.coop}/car/${this.matricule}/place_reserved/${this.params.date}`;
    this.placeProvider.customPath(pl_path);
    this.placeProvider.fetch(this.params.hour).then((place : Place)=>{
      console.log(place)
      if (place && place.reserved) {        
        occupeed = JSON.parse(place.reserved)
      }
      for( let key in this.selected) {
        occupeed.push(this.selected[key]);
      }
      let _place = new Place();
      _place.reserved = JSON.stringify(occupeed);
      this.placeProvider.customPath(pl_path);
      this.placeProvider.save(_place,this.params.hour);      
    })
  }

  
  public delete_vente(qrcode: string ) {
      //delete vente
      //is brooker
      this.brokerProvider.fetch(this.user.id).then((broker: Broker)=>{ 
        if (broker) {
            this.saleProvider.customPath(`broker/${this.user.id}/sale`);
            this.saleProvider.remove(qrcode);
        }
        else{
          //broker for cooperative
          this.requestProvider.customPath(`cooperative/${this.coop}/broker_request`);
          this.requestProvider.fetch(this.user.id).then((broker: Broker)=>{
            if (broker) {
              this.saleProvider.customPath(`cooperative/${this.coop}/broker_request/${this.user.id}/sale`);
              this.saleProvider.remove(qrcode);
            }
          })
        }
      })
  }

  presentAlert(message:string , title:string ="", status:boolean) {
     this.alertCtrl.create({
      title:  title,
      message: message,
      cssClass: 'alertCss',
      buttons: [{
        text: 'ok',
        handler: () => {
        }
      }]
    }).present();
  }

   seatstatus(seatPos){
    let cpath = `${this.path}/${this.class}/reservation`;
    let reserver;
    this.placeProvider.customPath(cpath);
    this.placeProvider.fetcAll().subscribe((booking)=>{
      if (booking) {
        for (let uid in booking) {
          for (let r in booking[uid]) {
            if (booking[uid][r].car === this.matricule && booking[uid][r].date === this.params.date ) {
              let occupe:any = [];
              occupe = JSON.parse(booking[uid][r].placelist);
              for(let o in occupe){
                if (occupe[o] === seatPos) {
                      if (uid === this.user.id) {
                        this.selected.push(seatPos);
                      }else {
                        this.reserved.push(seatPos);
                      }
                }
              }
              
            }
            
          }
        } 
      
      }
    });
  }


}
