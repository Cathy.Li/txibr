import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the MapPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-map',
  templateUrl: 'map.html',
})
export class MapPage {

  origin;
  destination;
  constructor(public navCtrl: NavController, public navParams: NavParams) {
  	this.origin = this.navParams.get('origin');
  	this.destination = this.navParams.get('destination')
  }

  ionViewDidLoad() {
    
  }

}
