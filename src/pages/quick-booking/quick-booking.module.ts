import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { QuickBookingPage } from './quick-booking';

@NgModule({
  declarations: [
    QuickBookingPage,
  ],
  imports: [
    IonicPageModule.forChild(QuickBookingPage),
  ],
})
export class QuickBookingPageModule {}
