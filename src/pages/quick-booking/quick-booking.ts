import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams,AlertController } from 'ionic-angular';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { StationProvider } from '../../providers/station/station';
import { Station } from '../../models/station.model';
import { Request } from '../../models/request.model';
import { AvailableBookingPage } from '../available-booking/available-booking';
import { PopUpProvider } from '../../providers/pop-up/pop-up';
import { DatePicker } from '@ionic-native/date-picker';
import { EventProvider } from '../../providers/event/event'

@IonicPage()
@Component({
   selector: 'page-quick-booking',
   templateUrl: 'quick-booking.html',
})
export class QuickBookingPage {

   form: FormGroup;
   cities = [];
   selectedDate: Date = new Date();
   customer$: string;
   now: any;

   constructor(
   	private formBuilder: FormBuilder,
   	private stationProvider: StationProvider,
      private navCtrl: NavController,
      private navParams: NavParams,
      private popupProvider: PopUpProvider,
      private datePicker: DatePicker,
      private eventProvider: EventProvider,
      private alertCtrl: AlertController,
      ){
      this.form = this.formBuilder.group({
         startlocation: ['',Validators.required],
         destination: ['',Validators.required],
      });
      /*this.init();
      this.now = new Date();

      // this.customer$ = this.navParams.get('customer$');
      this.customer$ = '2241575522759918';
      this.eventProvider.setEvent('customer$',{customer$: this.customer$});*/


   }

   ngOnInit(){
      this.init();
      this.now = new Date();

      this.customer$ = this.navParams.get('customer$');
      // this.customer$ = '2241575522759918';
      this.eventProvider.setEvent('customer$',{customer$: this.customer$});
   }

   init(){
      this.stationProvider.fetcAll().subscribe((stations)=>{

         this.popupProvider.showLoading();
         this.cities = [];

         for(let s in stations){

            let station = new Station(stations[s]);

            let in_cities = this.cities.find( city => city ==  station.city);

            if (!in_cities) {
               this.cities.push(station.city);
            }

         }
         this.popupProvider.dismissLoading();

      })
   }

   onSubmit(){

   	if (this.form.valid) {
         let request = new Request(this.form.value);
         this.navCtrl.push(AvailableBookingPage, {request: request, customer$: this.customer$});
      }


   }


}
