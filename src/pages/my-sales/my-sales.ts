import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { SaleProvider } from '../../providers/sale/sale';
import { Sale } from '../../models/sale.model';
import { QRCode } from '../../models/qrcode.model';
import { QrcodeProvider } from '../../providers/qrcode/qrcode';
import { BookingClassProvider } from '../../providers/booking-class/booking-class';
import { CooperativeProvider } from '../../providers/cooperative/cooperative';
import { ReservationProvider } from '../../providers/reservation/reservation';
import { TrajectProvider } from '../../providers/traject/traject';
import { StationProvider } from '../../providers/station/station';
import { BillingProvider } from '../../providers/billing/billing';
import { PopUpProvider } from '../../providers/pop-up/pop-up';

import { BrokerProvider } from '../../providers/broker/broker';
import { Broker } from '../../models/broker.model';
import { BrokerRequestProvider } from '../../providers/broker-request/broker-request';

/**
 * Generated class for the MySalesPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-my-sales',
  templateUrl: 'my-sales.html',
})
export class MySalesPage {

  broker$: string;
  mySales = [];
  daysWeek = ['Lundi','Mardi','Mercredi','Jeudi','Vendredi','Samedi','Dimanche']
  constructor(
  	public navCtrl: NavController,
  	public navParams: NavParams,
  	private saleProvider: SaleProvider,
  	private qrcodeProvider: QrcodeProvider,
  	private bookingClassProvider: BookingClassProvider,
  	private cooperativeProvider: CooperativeProvider,
  	private reservationProvider: ReservationProvider,
  	private trajetProvider: TrajectProvider,
  	private stationProvider: StationProvider,
  	private billingProvider: BillingProvider,
    private popuprovider: PopUpProvider,
    private brokerProvider: BrokerProvider,
    private requestProvider: BrokerRequestProvider
  	 ) {
  	this.broker$ = this.navParams.get('customer$');
    // this.broker$ = "2241575522759916";
    this.popuprovider.showLoading();
  	// this.init();
  }

  ngOnInit(){
      this.init();
  }

  init(){

    this.brokerProvider.fetch(this.broker$).then((broker: Broker)=>{ 
      if (broker) {
        //broker for all
        this.saleProvider.customPath(`broker/${this.broker$}/sale`);
        this.saleProvider.fetcAll().subscribe((mySales: Sale[])=>{
          if (mySales) {
            this.mySales = [];
            for(let key in mySales){ 
              let sale = mySales[key];
              this.qrcodeProvider.fetch(sale.qrReservation).then((qrcode: QRCode)=>{
                // this.bookingClassProvider.customPath(`cooperative/${qrcode.cooperative}/booking_class`);
                this.bookingClassProvider.customPath(`cooperative/${qrcode.cooperative}/planning/${qrcode.day}/${qrcode.hour}/${qrcode.trajet}`);
                this.bookingClassProvider.fetch(qrcode.class).then((bookingClass)=>{
                  this.cooperativeProvider.fetch(qrcode.cooperative).then((cooperative)=>{
                    this.reservationProvider.customPath(`cooperative/${qrcode.cooperative}/planning/${qrcode.day}/${qrcode.hour}/${qrcode.trajet}/${qrcode.class}/reservation/${qrcode.uid}`);
                    this.reservationProvider.fetch(qrcode.reservation).then((reservation)=>{
                      this.trajetProvider.customPath(`cooperative/${qrcode.cooperative}/trajet`);
                      this.trajetProvider.fetch(qrcode.trajet).then((trajet)=>{
                        this.stationProvider.fetch(trajet['depart']).then((startstation)=>{
                          this.stationProvider.fetch(trajet['arrive']).then((arrivalstation)=>{
                            this.billingProvider.customPath(`billing/${qrcode.uid}`);
                            this.billingProvider.fetch(reservation['billing']).then((billing)=>{
                              let details = {};
                              let days = Number(qrcode.day)-1;
                              if (0>days) {
                                days = 6;
                              }
                              details = {
                                bookingClass: bookingClass, 
                                cooperative: cooperative,
                                //day: this.daysWeek[Number(qrcode.day)-1],
                                day: this.daysWeek[days],
                                hour: qrcode.hour,
                                reservation: reservation,
                                startstation: startstation,
                                arrivalstation: arrivalstation,
                                billing: billing,
                                benefit: sale.benefit,
                                status: sale.status                          
                              };
                              this.mySales.push(details);
                              this.popuprovider.dismissLoading();
                            });
                          });
                        });
                      });
                    });
                  });
                });
              });
            }
          }
          else{
            this.popuprovider.dismissLoading();
          }
        });
      }else{
        this.cooperativeProvider.fetcAll().subscribe((cooperatives)=>{
          for(let c in cooperatives){
            //broker for cooperative
            this.requestProvider.customPath(`cooperative/${c}/broker_request`);
            this.requestProvider.fetch(this.broker$).then((broker: Broker)=>{
              if (broker) {
                this.saleProvider.customPath(`cooperative/${c}/broker_request/${this.broker$}/sale`);
                this.saleProvider.fetcAll().subscribe((mySales: Sale[])=>{
                  if (mySales) {
                    this.mySales = [];
                    for(let key in mySales){ 
                      let sale = mySales[key];
                      this.qrcodeProvider.fetch(sale.qrReservation).then((qrcode: QRCode)=>{
                        // this.bookingClassProvider.customPath(`cooperative/${qrcode.cooperative}/booking_class`);
                        this.bookingClassProvider.customPath(`cooperative/${qrcode.cooperative}/planning/${qrcode.day}/${qrcode.hour}/${qrcode.trajet}`);
                        this.bookingClassProvider.fetch(qrcode.class).then((bookingClass)=>{
                          this.cooperativeProvider.fetch(qrcode.cooperative).then((cooperative)=>{
                            this.reservationProvider.customPath(`cooperative/${qrcode.cooperative}/planning/${qrcode.day}/${qrcode.hour}/${qrcode.trajet}/${qrcode.class}/reservation/${qrcode.uid}`);
                            this.reservationProvider.fetch(qrcode.reservation).then((reservation)=>{
                              this.trajetProvider.customPath(`cooperative/${qrcode.cooperative}/trajet`);
                              this.trajetProvider.fetch(qrcode.trajet).then((trajet)=>{
                                this.stationProvider.fetch(trajet['depart']).then((startstation)=>{
                                  this.stationProvider.fetch(trajet['arrive']).then((arrivalstation)=>{
                                    this.billingProvider.customPath(`billing/${qrcode.uid}`);
                                    this.billingProvider.fetch(reservation['billing']).then((billing)=>{
                                      let details = {};
                                      let days = Number(qrcode.day)-1;
                                      if (0>days) {
                                        days = 6;
                                      }
                                      details = {
                                        bookingClass: bookingClass, 
                                        cooperative: cooperative,
                                        //day: this.daysWeek[Number(qrcode.day)-1],
                                        day: this.daysWeek[days],
                                        hour: qrcode.hour,
                                        reservation: reservation,
                                        startstation: startstation,
                                        arrivalstation: arrivalstation,
                                        billing: billing,
                                        benefit: sale.benefit,
                                        status: sale.status                          
                                      };
                                      this.mySales.push(details);
                                      this.popuprovider.dismissLoading();
                                    });
                                  });
                                });
                              });
                            });
                          });
                        });
                      });
                    }
                  }
                  else{
                    this.popuprovider.dismissLoading();
                  }
                });
              }
            })
          }
          //end for cooperatives
        })
        //end list cooperatives
      }
      //end else
      this.popuprovider.dismissLoading();
    });
  	
  }

}
