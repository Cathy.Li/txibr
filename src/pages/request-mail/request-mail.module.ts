import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { RequestMailPage } from './request-mail';

@NgModule({
  declarations: [
    RequestMailPage,
  ],
  imports: [
    IonicPageModule.forChild(RequestMailPage),
  ],
})
export class RequestMailPageModule {}
