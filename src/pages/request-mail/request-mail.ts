import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController } from 'ionic-angular';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

/**
 * Generated class for the RequestMailPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-request-mail',
  templateUrl: 'request-mail.html',
})
export class RequestMailPage {

  form: FormGroup;

  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    public viewCtrl: ViewController,
    public formBuilder: FormBuilder) {

      this.form = this.formBuilder.group({
        mail: ['', Validators.email]
      });
  }

  ionViewDidLoad() {
  }

  onSubmit(){
    if(this.form.valid){
      let value = this.form.value;
      this.viewCtrl.dismiss(value.mail);
    }
  }

}
