import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { BrokerRegisterPage } from './broker-register';

@NgModule({
  declarations: [
    BrokerRegisterPage,
  ],
  imports: [
    IonicPageModule.forChild(BrokerRegisterPage),
  ],
})
export class BrokerRegisterPageModule {}
