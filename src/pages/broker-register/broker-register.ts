import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { BrokerProvider } from '../../providers/broker/broker';
import { Broker } from '../../models/broker.model';
import { PopUpProvider } from '../../providers/pop-up/pop-up';

import { BrokerMenuPage } from '../../pages/broker-menu/broker-menu';

/**
 * Generated class for the BrokerRegisterPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-broker-register',
  templateUrl: 'broker-register.html',
})
export class BrokerRegisterPage {

  form: FormGroup;
  customer$: string;
  constructor(
  	public navCtrl: NavController, 
  	public navParams: NavParams,
  	private formBuilder: FormBuilder,
  	private brokerProvider: BrokerProvider,
  	private popupProvider: PopUpProvider
  	) {
  	this.form = this.formBuilder.group({
 			name: ['',Validators.required],
 			address: ['',Validators.required],
 			contact: ['',Validators.required]
 		});
  	this.customer$ = this.navParams.get('customer$');
  }

  ionViewDidLoad() {
    
  }

  onSubmit(){
  	if (this.form.valid) {
  		this.popupProvider.presentConfirm('Voulez-vous envoyer la formulaire de démande pour devenir agent de TAXIBR ?').then((ok)=>{
  			this.form.value.status = false;
  			let broker = new Broker(this.form.value);
  			this.brokerProvider.save(broker,this.customer$);
        this.navCtrl.push(BrokerMenuPage,{customer$: this.customer$});
  		}, (cancel)=>{})
  	}
  }

}
