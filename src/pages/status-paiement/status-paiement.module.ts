import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { StatusPaiementPage } from './status-paiement';

@NgModule({
  declarations: [
    StatusPaiementPage,
  ],
  imports: [
    IonicPageModule.forChild(StatusPaiementPage),
  ],
})
export class StatusPaiementPageModule {}
