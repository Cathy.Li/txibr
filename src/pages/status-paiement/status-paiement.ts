import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController, Loading  } from 'ionic-angular';


import { AccountDetailPage } from '../account-detail/account-detail';

import { PaiementMethodPage } from '../paiement-method/paiement-method';

import { StorageProvider } from '../../providers/storage/storage';
import { Session } from '../../lib/models/session';
import { Account } from '../../lib/models/account';
import { AccountProvider } from '../../providers/cyclos/account';
/**
 * Generated class for the StatusPaiementPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-status-paiement',
  templateUrl: 'status-paiement.html',
})
export class StatusPaiementPage {


  id: any;
  params: any;
    availableBalance: string;
    session : Session;
    loading: Loading;

  constructor(public navCtrl: NavController,
      private storagePrvd:StorageProvider,
      private loadingCtrl: LoadingController,
      public accountPrd: AccountProvider, public navParams: NavParams) {
    // this.init();
  }

  ngOnInit(){
      this.init();
  }

  init(){
    this.params = this.navParams.get('params');
    this.id = this.navParams.get('id');
    //this.id = '1';
  }

  ionViewDidLoad() {
    this.presentLoading()

    this.storagePrvd.getSession().then((session :Session) => {

      this.session = session;

      this.accountPrd.getAccountType(session).then((account:Account) => {
        
        this.availableBalance = account.status.availableBalance
        this.loading.dismiss()
      }).catch((error) => {
        console.log(error)
        this.loading.dismiss()
      })
    }).catch((error) => {
      console.log(error)
      this.loading.dismiss()
    })
  }


  protected presentLoading() {
      this.loading =  this.loadingCtrl.create()
      return this.loading.present()
  }

  paiementOk(){
    this.navCtrl.setRoot(AccountDetailPage,{ session: this.session , availableBalance : this.availableBalance});
  }

  refreshPaiement(){
    this.navCtrl.push(PaiementMethodPage, {params: this.params});
  }

}
