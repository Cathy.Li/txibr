import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AvailableBookingPage } from './available-booking';

@NgModule({
  declarations: [
    AvailableBookingPage,
  ],
  imports: [
    IonicPageModule.forChild(AvailableBookingPage),
  ],
})
export class AvailableBookingPageModule {}
