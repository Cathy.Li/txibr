import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams  } from 'ionic-angular';
import { Request } from '../../models/request.model';
import { StationProvider } from '../../providers/station/station';
import { CooperativeProvider } from '../../providers/cooperative/cooperative';
import { PopUpProvider } from '../../providers/pop-up/pop-up';
import { AvailableCarPage } from '../available-car/available-car';
import { PlanningProvider } from '../../providers/planning/planning';
import { TrajectProvider } from '../../providers/traject/traject' ;
import { BookingClassProvider } from '../../providers/booking-class/booking-class';
import { EventProvider } from '../../providers/event/event';
import { ModalController } from 'ionic-angular';
import { CalendarProvider } from '../../providers/calendar/calendar';
import { WorkerProvider } from '../../providers/worker/worker';
import { Worker } from '../../models/worker.model';
import { Cooperative } from '../../models/cooperative.model';

import { Broker } from '../../models/broker.model';
import { BrokerRequestProvider } from '../../providers/broker-request/broker-request';
import { BrokerProvider } from '../../providers/broker/broker';
import { MyCooperative } from '../../models/mycooperative.model';
import { UtilProvider } from '../../providers/util/util';




@IonicPage()
@Component({
  selector: 'page-available-booking',
  templateUrl: 'available-booking.html',
})
export class AvailableBookingPage {

  request: Request;
  bookings = [];
  empty: boolean = true;
  customer$: string;
  date: string;
  type: 'string';
  daysTravel = [];
  disableWeeks = [];
  weeks = [1,2,3,4,5,6,0];
  selectedDate: any;
  selectedDay: number;
  ok: boolean = false;
  isCooperativeBroker: boolean = false;
  myReservation: any;
  myCooperative:  string[] = [];
  myCooperatives = [];
  is_bro:any;


  constructor(
    private navCtrl: NavController,
    private navParams: NavParams,
    private stationProvider: StationProvider,
    private cooperativeProvider: CooperativeProvider,
    private popupProvider: PopUpProvider,
    private planningProvider: PlanningProvider,
    private trajectProvider: TrajectProvider,
    private bookingclassProvider: BookingClassProvider,
    private eventProvider: EventProvider,
    private modalCtrl: ModalController,
    private calendarProvider: CalendarProvider,
    private workerProvider: WorkerProvider,
    private requestProvider: BrokerRequestProvider,
    private brokerProvider: BrokerProvider,
    private utilProvider: UtilProvider

    ){
    /*this.popupProvider.showLoading();

    //this.finPlanning();
    this.customer$ = this.navParams.get('customer$');
    // this.customer$ = "2241575522759919";
    this.eventProvider.setEvent('customer$',{customer$: this.customer$});
    this.init();*/

  }



  ngOnInit(){
    this.customer$ = this.navParams.get('customer$');
    this.eventProvider.setEvent('customer$',{customer$: this.customer$});
    this.init();
  }



  init(){
    this.myReservation = this.navParams.get('request');
    
    if(this.myReservation.reservation$ && this.myReservation.matricule){
      this.cooperativeProvider.fetch(this.myReservation.cooperative).then((cooperative)=>{
        this.requestProvider.customPath(`cooperative/${this.myReservation.cooperative}/broker_request`);
        this.requestProvider.fetch(this.customer$).then((broker: Broker)=>{
          if (broker) {
            this.disableWeeks = this.weeks;
            let coop = {key: this.myReservation.cooperative};
            // coop = cooperative;
            // coop['key'] = this.myReservation.cooperative;
            // this.myCooperatives.push(coop);
            this.findPlanningInCooperative(coop);
          }else{
            this.finPlanningWithCoop();
          }
        })
      })
    
    }else {
      this.utilProvider.is_broker_CVie(this.customer$).then((res) => {
        if (res.length>0) { 
          for (let k in res) {
            let coop = {key:res[k]};
            this.disableWeeks = this.weeks;
           this.findPlanningInCooperative(coop);
          }

        } else {
          this.finPlanning();
        }
      });
    }

      
  }


  openCalendar(){
    //this.popupProvider.PresentToast('Action terminé');
    //console.log(this.disableWeeks.length);
    if (this.disableWeeks.length == 7) {
      this.popupProvider.PresentToast('Aucun voyage disponible pour ce trajet');
    }
    else{
      let date_marker = this.myReservation.date ? new Date(this.myReservation.date) : '';
        this.calendarProvider.open(this.disableWeeks,date_marker).then((date)=>{
        if (date) {
          this.selectedDate = date['string'];
          this.selectedDay = new Date(this.selectedDate).getDay();
          this.ok = true;
        }
      });
    }
  }

  findPlanningInCooperative(cooperative){
    this.daysTravel = [];
    this.request = this.navParams.get('request');
    // this.disableWeeks = this.weeks;
    this.bookings = [];
    let c = cooperative.key;
    this.planningProvider.customPath(`cooperative/${c}/planning`);
    this.planningProvider.fetcAll().subscribe((plannings)=>{
      for(let d in plannings){
        for(let h in plannings[d]){
          for(let t in plannings[d][h]){
            this.trajectProvider.customPath(`cooperative/${c}/trajet`);
            this.trajectProvider.fetch(t).then((trajet)=>{
              if (trajet) {
                this.stationProvider.fetch(trajet['depart']).then((startstation)=>{
                  this.stationProvider.fetch(trajet['arrive']).then((arrivalstaton)=>{
                    if ((startstation['city'] == this.request.startlocation) && (arrivalstaton['city'] == this.request.destination)) {
                      let day = parseInt(d);
                      if (day == 7) {
                        day = 0;
                      }
                      this.disableWeeks.splice(this.disableWeeks.indexOf(day),1);
                      for(let bc in plannings[d][h][t]){
                        //this.bookingclassProvider.customPath(`cooperative/${c}/booking_class`);
                        //this.bookingclassProvider.fetch(bc).then((bookingclass)=>{
                          let planning = {};
                          planning['cooperative'] = cooperative;
                          planning['cooperative'].key = c;
                          planning['trajet'] = trajet;
                          planning['trajet'].startstation = startstation;
                          planning['trajet'].arrivalstation = arrivalstaton;
                          //planning['class'] = bookingclass;
                          //planning['price'] = trajet['price'][bc]['price'];
                          planning['price'] = trajet['price'];
                          planning['key'] = bc;
                          planning['hour'] = h;
                          planning['day'] = parseInt(d);
                          planning['params'] = {
                            class: bc,
                            day: d,
                            hour: h,
                            coop: c,
                            trajet: trajet,
                            trajet$: t,
                            path: `cooperative/${c}/planning/${d}/${h}/${t}`,
                            //price: trajet['price'][bc]['price']
                            price: trajet['price']
                          };
                          this.bookings.push(planning);
                        //});
                      }
                    }
                  });
                });
              }
            });
          }
        }
      }
      this.popupProvider.dismissLoading()
    });
  }

  sortByPrice(key1: any, key2: any){
    return Math.min(key1.price, key2.price);
  }

  sort(){
    this.bookings.sort(this.sortByPrice);
  }


  finPlanning(){
    this.daysTravel = [];
    this.request = this.navParams.get('request');
    this.cooperativeProvider.fetcAll().subscribe((cooperatives)=>{
      this.disableWeeks = this.weeks;
      this.bookings = [];
      for(let c in cooperatives){
        this.cooperativeProvider.fetch(c).then((cooperative)=>{
          this.planningProvider.customPath(`cooperative/${c}/planning`);
          //this.planningProvider.customPath(`voyage`);
          this.planningProvider.fetcAll().subscribe((plannings)=>{
            for(let d in plannings){
              for(let h in plannings[d]){

                for(let t in plannings[d][h]){
                  this.trajectProvider.customPath(`cooperative/${c}/trajet`);
                  this.trajectProvider.fetch(t).then((trajet)=>{

                    if (trajet) {
                      this.stationProvider.fetch(trajet['depart']).then((startstation)=>{
                        if (startstation) {
                          this.stationProvider.fetch(trajet['arrive']).then((arrivalstaton)=>{
                            if ((startstation['city'] == this.request.startlocation) && (arrivalstaton['city'] == this.request.destination)) {
                              let day = parseInt(d);
                              if (day == 7) {
                                day = 0;
                              }
                              this.disableWeeks.splice(this.disableWeeks.indexOf(day),1);
                              for(let bc in plannings[d][h][t]){
                                //this.bookingclassProvider.customPath(`cooperative/${c}/booking_class`);
                                //this.bookingclassProvider.fetch(bc).then((bookingclass)=>{
                                 // console.log(bookingclass);
                                  let planning = {};
                                  planning['cooperative'] = cooperative;
                                  planning['cooperative'].key = c;
                                  planning['trajet'] = trajet;
                                  planning['trajet'].startstation = startstation;
                                  planning['trajet'].arrivalstation = arrivalstaton;
                                 // planning['class'] = bookingclass;
                                  //planning['price'] = trajet['price'][bc]['price'];
                                  planning['price'] = trajet['price'];
                                  planning['key'] = bc;
                                  planning['hour'] = h;
                                  planning['day'] = parseInt(d);
                                  planning['params'] = {
                                    class: bc,
                                    day: d,
                                    hour: h,
                                    coop: c,
                                    trajet: trajet,
                                    trajet$: t,
                                    path: `cooperative/${c}/planning/${d}/${h}/${t}`,
                                    //price: trajet['price'][bc]['price']
                                    price: trajet['price']
                                  };
                                  this.bookings.push(planning);
                                //});
                              }
                            }
                          });
                        }
                      });
                    }
                  });
                }
              }
            }
          });
        })
      }
      this.popupProvider.dismissLoading();
    });
  }


  finPlanningWithCoop(){
    this.daysTravel = [];
    this.request = this.navParams.get('request');
      this.disableWeeks = this.weeks;
      this.bookings = [];
        this.cooperativeProvider.fetch(this.myReservation.cooperative).then((cooperative)=>{
          this.planningProvider.customPath(`cooperative/${this.myReservation.cooperative}/planning`);
          //this.planningProvider.customPath(`voyage`);
          this.planningProvider.fetcAll().subscribe((plannings)=>{
            for(let d in plannings){
              for(let h in plannings[d]){

                for(let t in plannings[d][h]){
                  this.trajectProvider.customPath(`cooperative/${this.myReservation.cooperative}/trajet`);
                  this.trajectProvider.fetch(t).then((trajet)=>{

                    if (trajet) {
                      this.stationProvider.fetch(trajet['depart']).then((startstation)=>{
                        if (startstation) {
                          this.stationProvider.fetch(trajet['arrive']).then((arrivalstaton)=>{
                            if ((startstation['city'] == this.request.startlocation) && (arrivalstaton['city'] == this.request.destination)) {
                              let day = parseInt(d);
                              if (day == 7) {
                                day = 0;
                              }
                              this.disableWeeks.splice(this.disableWeeks.indexOf(day),1);
                              for(let bc in plannings[d][h][t]){
                                //this.bookingclassProvider.customPath(`cooperative/${c}/booking_class`);
                                //this.bookingclassProvider.fetch(bc).then((bookingclass)=>{
                                 // console.log(bookingclass);
                                  let planning = {};
                                  planning['cooperative'] = cooperative;
                                  planning['cooperative'].key = this.myReservation.cooperative;
                                  planning['trajet'] = trajet;
                                  planning['trajet'].startstation = startstation;
                                  planning['trajet'].arrivalstation = arrivalstaton;
                                 // planning['class'] = bookingclass;
                                  //planning['price'] = trajet['price'][bc]['price'];
                                  planning['price'] = trajet['price'];
                                  planning['key'] = bc;
                                  planning['hour'] = h;
                                  planning['day'] = parseInt(d);
                                  planning['params'] = {
                                    class: bc,
                                    day: d,
                                    hour: h,
                                    coop: this.myReservation.cooperative,
                                    trajet: trajet,
                                    trajet$: t,
                                    path: `cooperative/${this.myReservation.cooperative}/planning/${d}/${h}/${t}`,
                                    //price: trajet['price'][bc]['price']
                                    price: trajet['price']
                                  };
                                  this.bookings.push(planning);
                                //});
                              }
                            }
                          });
                        }
                      });
                    }
                  });
                }
              }
            }
          });
        })
      this.popupProvider.dismissLoading();
  }

  selectBooking(params){
    params.date = this.selectedDate;
    params.isCooperativeBroker = this.isCooperativeBroker;
    this.navCtrl.push(AvailableCarPage, { params: params, request: this.myReservation });
  }

}
