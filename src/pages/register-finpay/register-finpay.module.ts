import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { RegisterFinpayPage } from './register-finpay';

@NgModule({
  declarations: [
    RegisterFinpayPage,
  ],
  imports: [
    IonicPageModule.forChild(RegisterFinpayPage),
  ],
})
export class RegisterFinpayPageModule {}
