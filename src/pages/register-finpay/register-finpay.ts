import { Component, OnInit } from '@angular/core';
import { IonicPage, ToastController, LoadingController, AlertController, NavController, NavParams } from 'ionic-angular';
import { FinpayFirebaseProvider } from '../../providers/finpay-firebase/finpay-firebase';
import { OrchidProvider } from '../../providers/orchid/orchid';
import { PictUploadPage } from '../pict-upload/pict-upload';
import { Individual } from '../../lib/entities/individual';
import {  FormGroup} from '@angular/forms';
import { AuthProvider } from '../../providers/cyclos/auth';
/**
 * Generated class for the RegisterFinpayPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-register-finpay',
  templateUrl: 'register-finpay.html',
})
export class RegisterFinpayPage {
signupform: FormGroup;
  id : string ;
  userData = { "email": ""};

  icon = {
    url: './assets/icon/folders.png',
    alt: 'icon folders',
    //label: 'Selfie'
  }
  params : any ;

  ngOnInit() {
  }

	protected customer: any = {
    phone: '',
    //phone: '0347442059',
    cin: '',
    //cin: '101081057023',
    account: '',
    //account: '0002500166',
    email:'',
  };
	protected phoneValid: boolean = false;
	protected emailValid:boolean = false;
	protected cinValid: boolean = false;
	protected accountValid: boolean = false;
	protected newCustomer: boolean = false;

  constructor(
    private auth:AuthProvider,
    private finpayfbPrvd: FinpayFirebaseProvider,
    private loadingCtrl: LoadingController,
  	private toastCtrl: ToastController,
    private orchidProvider: OrchidProvider,
    private alertCtrl: AlertController,
  	private navCtrl: NavController, 
    private navParams: NavParams) {

	  this.params = this.navParams.get('params');
  }

  infoLogin(infoLogin :any) {
    console.log(infoLogin);
    let individual:Individual = new Individual();
    console.log(this.customer)
    individual.setCin(this.customer.cin, true)
    individual.email = infoLogin.email
    individual.password = infoLogin.password
    individual.setPhone(this.customer.phone,true)

    let isCustomerRef = FinpayFirebaseProvider.CUSTOMER_REF + "/" + individual.phone
    this.finpayfbPrvd.notExistRef(isCustomerRef).then(() => {
      this.navCtrl.push(PictUploadPage, { customer :individual })
    }).catch(() => {
      //do nothing
    })
  }



  validateAccount() {
    //if (this.customer.account.length < 11) {
    if (this.customer.account.length < 10) {
      this.presentError('Le numéro de compte doit avoir au moins 10 chiffres')
      //this.presentError('Le numéro de compte doit avoir au moins 11 chiffres')
      return
    }

    let cin = this.customer.cin.split(" - ").join("")

    let loading = this.loadingCtrl.create();

    loading.present()

    this.orchidProvider
    .valideAccount(cin, this.customer.account)
    .then((valid) => {
      loading.dismiss()
      console.log(valid)
      if (!valid) {
        this.presentAlert('Compte invalide', 'Erreur')
        return
      }

      this.accountValid = true

    })
  }

  modify(item: string) {
    this[item + 'Valid'] = false
    this.cinValid = false
    this.newCustomer = false
  }

  verifyLength(item: string) {
    switch (item) {
      case "phone":
        if (this.customer.phone.length > 13) {
          this.customer.phone = this.customer.phone.slice(0, 13)
        }

        break;

      case "cin":
        if (this.customer.cin.length > 21) {
          this.customer.cin = this.customer.cin.slice(0, 21)
        }
        break;

      case "account":
        /*if (this.customer.account.length > 11) {
          this.customer.account = this.customer.account.slice(0, 11)
        }*/
        if (this.customer.account.length > 10) {
          this.customer.account = this.customer.account.slice(0, 10)
        }
        break;
    }
  }

  folders() {
    this.navCtrl.push(PictUploadPage, { pict: this.customer.pict })
  }

  validate(item: string) {
  	let func = 'validate' + item
  	this[func]()
  }
  validateEmail(){
     if (this.customer.email.valid){
       this.presentError('Le numéro CIN doit avoir au moins 12 chiffres')
        return
     }
  }

  validateCin() {
   
    let customer_cin = this.customer.cin;
    let sixSt = customer_cin[8];

    if (sixSt != 1 && sixSt !=2) {
      this.presentError('Le 6 ème chiffre du numéro CIN doît être égale à 1 ou 2')
      return
    }

    let onest = customer_cin[0];

    if (onest < 1  || onest > 6) {
    this.presentError('Le 1 ère chiffre du numéro CIN doît être entre 1 et 6')
    return
    }

    if (this.customer.cin.length < 21) {
      this.presentError('Le numéro CIN doit avoir au moins 12 chiffres')
      return
    }

    this.auth.isValideField("cin",customer_cin.split(" - ").join("")).then((response:any) => {
      console.log("success")
      console.log(response)
      console.log(response.status)
      if(response.status == 200) {
        console.log(response._body)
        this.presentError(response._body)
        return
      }else{
        this.cinValid = true
        this.newCustomer = true
      }
    })

  }

   validatePhone() {

     let phon_number = this.customer.phone;
     let oneSt = phon_number[0];
     let twoSt = phon_number[1];

   if (oneSt != 0 || twoSt != 3)
    {

      this.presentError('Le numéro de téléphone commence par 03')
        return
     }

  	if (this.customer.phone.length < 13) {
      this.presentError('Le numéro de téléphone doit avoir au moins 10 chiffres')
      return
    }
    console.log(phon_number.split(" ").join(""))
    this.auth.isValideField("username",phon_number.split(" ").join("")).then((response:any) => {
      console.log("success")
      console.log(response)
      console.log(response.status)
      if(response.status == 200) {
        console.log(response._body)
        this.presentError('Le numéro de téléphone ou ' + response._body)
        return
      }else{
        this.phoneValid = true
      }
    })

  }

  protected presentAlert(message: string, title: string = 'Attention') {
    this.alertCtrl.create({
      title: title,
      message: message,
      buttons: [{
        text: 'OK'
      }]
    }).present();
  }

  public presentError(message: string) {
    let toast = this.toastCtrl.create({
      message: message,
      duration: 3000,
      position: 'bottom',
    });

    toast.onDidDismiss(() => {
      
    });

    toast.present();
  }



  login(){
    this.navCtrl.pop();
  }

  return() {
    this.navCtrl.pop();
  }


}
