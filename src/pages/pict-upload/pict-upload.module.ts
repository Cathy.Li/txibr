import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PictUploadPage } from './pict-upload';


import { AngularFireModule } from "angularfire2";
import { FINPAY_FIREBASE_CONFIG } from '../../app/app.firebase.config';

@NgModule({
  declarations: [
    PictUploadPage,
  ],
  imports: [
    IonicPageModule.forChild(PictUploadPage),
    //AngularFireModule.initializeApp(FINPAY_FIREBASE_CONFIG)
  ],
})
export class PictUploadPageModule {}
