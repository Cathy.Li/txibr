import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PrinterWidgetPage } from './printer-widget';

@NgModule({
  declarations: [
    PrinterWidgetPage,
  ],
  imports: [
    IonicPageModule.forChild(PrinterWidgetPage),
  ],
})
export class PrinterWidgetPageModule {}
