import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController } from 'ionic-angular';
import { PrinterProvider } from '../../providers/printer/printer';
import { AvailableBluetoothPrinterPage } from '../available-bluetooth-printer/available-bluetooth-printer'

/**
 * Generated class for the PrinterWidgetPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

 @IonicPage()
 @Component({
 	selector: 'page-printer-widget',
 	templateUrl: 'printer-widget.html',
 })
 export class PrinterWidgetPage {

 	htmlContent: any;
 	content: any;
 	qrcodeHex: any;
 	constructor(
 		public navCtrl: NavController, 
 		public navParams: NavParams,
 		private printerProvider: PrinterProvider,
 		private viewCtrl: ViewController
 		) {
 		this.init();
 	}

 	init(){
 		this.htmlContent = this.navParams.get('htmlContent');
 		this.content = this.navParams.get('content');
 	}

 	ionViewDidLoad() {
 		
 	}

 	processPrint(){
 		this.printerProvider.print(this.htmlContent);
 		this.viewCtrl.dismiss();
 	}

 	goToAvailableBTPriner(){
 		this.navCtrl.push(AvailableBluetoothPrinterPage,{content:this.content});
 		this.viewCtrl.dismiss();
 	}

 }
