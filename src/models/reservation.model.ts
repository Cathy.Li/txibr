export class Reservation {
	
	nbplace: number;
	placelist: string;
	car: string;
	status: string;
	amount: number;
	customer: string;
	date?: string;
	billing: string;
	qrcode: string;
	rest: number;

	constructor(object?: {}) {
		for (var key in object) {
			this[key] = object[key];
		}
	}
}