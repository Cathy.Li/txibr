export class Request {
	
	startlocation: string;
	destination: string;
	date: string;
	hour ?: string;
	day: string;
	
	constructor(object?: {}) {
		for (var key in object) {
			this[key] = object[key];
		}
	}
}