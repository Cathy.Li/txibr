export class Billing  {
	
	name: string;
	address: string;
	name_reserver: string;
	cin_reserver: string;
	
	constructor(object?: {}) {
		for (var key in object) {
			this[key] = object[key];
		}
	}
}