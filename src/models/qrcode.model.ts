export class QRCode  {
	
	cooperative: string;
	day: string;
	hour: string;
	trajet: string;
	class: string;
	uid: string;
	reservation: string;
	
	constructor(object?: {}) {
		for (var key in object) {
			this[key] = object[key];
		}
	}
}