export class Sale  {
	qrReservation: string;
	benefit: number;
	status: boolean;	
	constructor(object?: {}) {
		for (var key in object) {
			this[key] = object[key];
		}
	}
}