export class Broker  {
	name: string;
	address: string;
	contact: string;
	status: boolean;	
	constructor(object?: {}) {
		for (var key in object) {
			this[key] = object[key];
		}
	}
}