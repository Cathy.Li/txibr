import { Injectable } from '@angular/core';
import { AngularFireDatabase } from 'angularfire2/database';
import { FirebaseProvider } from '../firebase/firebase';
import { Place } from '../../models/place.model';
import { PopUpProvider } from '../../providers/pop-up/pop-up'

@Injectable()
export class PlaceProvider extends FirebaseProvider {

  protected __path = 'place_reserved';
  constructor(public firebase: AngularFireDatabase, public popupProvider : PopUpProvider) {
    super(firebase,popupProvider);
  }

  fetchOne(id: string){
    return this.getOne(id);
  }

  save(place: Place, key ?: string){
    if (key) {
      return this.push(place,key);
    }
    else{
      return this.push(place);
    }
  }

  customPath(path: string){
    this.setPath(path);
  }

  getCustomPath(){
    return this.getPath();
  }

  fetch(key: string){
    return this.getOneById(key);
  }

  fetcAll(){
    return this.getAll();
  }

}
