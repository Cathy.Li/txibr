import { Injectable } from '@angular/core';
import { Printer } from '@ionic-native/printer';
import { BluetoothSerial } from '@ionic-native/bluetooth-serial';
import { PopUpProvider } from '../pop-up/pop-up';
import { BluetoothProvider } from '../bluetooth/bluetooth';
import { EscPosCommandProvider } from '../esc-pos-command/esc-pos-command';

/*
  Generated class for the PrinterProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
  */
  @Injectable()
  export class PrinterProvider {

    printers: any;
    constructor(
      private bluetoothSerial: BluetoothSerial, 
      private printer: Printer,
      private popupProvider: PopUpProvider,
      private bluetoothProvider: BluetoothProvider,
      private escposProvider: EscPosCommandProvider
      ) {

    }

    print(content: any){
      this.printer.isAvailable()
      .then((success)=>{
        this.printer.print(content)
      })
      .catch((error)=>{
        this.popupProvider.dismissLoading();
        this.popupProvider.presentConfirm("Imprimante non disponible","Erreur")
      })
    }

    formalizeData(content: any){
      let text: string = '';
      for( let element in content ){
        if (content[element].label != 'qrcode') {
          if (content[element].label == '*') {
            text += content[element].value + '\n';
          }
          else{
            text += content[element].label + ':' + content[element].value + '\n';
          }
        }
        else{
          text += this.escposProvider.barcodeEncoder(content[element].value)
        }
      }
      return text ;
    }

    findAllBluetoothPrinters(){
      return this.bluetoothProvider.search();
    }

    refreshBluetoothPrintersList(refresher){
      return this.bluetoothProvider.refresh(refresher);
    }

    writeDataToBluetoothSerial(device: any, content: any){
      this.popupProvider.showLoading();
      this.bluetoothProvider.connect(device.id).then((connected)=>{
        if (connected) {
          let text = this.formalizeData(content);
          this.bluetoothProvider.write(text).then((success)=>{          
            this.popupProvider.presentAlert('Impréssion terminé');
            this.popupProvider.dismissLoading();
          });
        }
        else{
          this.popupProvider.dismissLoading();
        }

      }, (notconnected)=>{
        this.popupProvider.dismissLoading();
      }).then(()=>{
        this.bluetoothProvider.disconnect();
      })
    }

    escapeSpecialChar(text: string){
      return text.replace('é','e');
    }


  }
