import { Injectable } from '@angular/core';
import { QRScanner, QRScannerStatus } from '@ionic-native/qr-scanner';
import { App } from 'ionic-angular';

  @Injectable()
  export class QrScannerProvider {

  	constructor(public qrScanner: QRScanner, private app: App) {

  	}

  	showCamera() {
  		//(window.document.querySelector('ion-app') as HTMLElement).classList.add('cameraView');
      window.document.querySelector('ion-app').classList.add('cameraView');
  	}

  	hideCamera() {
  		//(window.document.querySelector('ion-app') as HTMLElement).classList.remove('cameraView');
      window.document.querySelector('ion-app').classList.remove('cameraView');
  	}

  	openScaner(page:any, params?:any){
  		this.qrScanner.prepare().then((status: QRScannerStatus)=>{

  			if (status.authorized) {
  				
  				this.showCamera();

  				let scanSub = this.qrScanner.scan().subscribe((data)=>{

  					//this.scanned(data);
  					this.hideCamera();

            this.qrScanner.hide();

            scanSub.unsubscribe();


  					this.app.getActiveNav().setRoot(page,{result:data, params: params || null});
  					
  				});

  				this.qrScanner.resumePreview();
  				this.qrScanner.show()
  				.then((data: QRScannerStatus)=>{

  				},
  				err =>{
  					alert(err)
  				});
  			}
  		})
  		.catch((error: any)=>{
  			alert(error)
  		})
  	}

  	scanned(data: any) {
  		window.open(data, '_blank');
  	}

  }
