import { Injectable } from '@angular/core';
import { AngularFireDatabase } from 'angularfire2/database';
import { FirebaseProvider } from '../firebase/firebase';
import { PopUpProvider } from '../../providers/pop-up/pop-up';

/*
  Generated class for the PathVoyageProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class TrajectProvider extends FirebaseProvider{

  protected __path = 'trajet';
  
  constructor(public firebase: AngularFireDatabase, public popupProvider : PopUpProvider) {
    super(firebase,popupProvider);
  }

  customPath(path:string){
    this.setPath(path)
  }

  fetch(key: string){
    return this.getOneById(key);
  }

  fetcAll(){
    return this.getAll();
  }

}
