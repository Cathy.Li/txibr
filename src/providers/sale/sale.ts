import { Injectable } from '@angular/core';
import { AngularFireDatabase } from 'angularfire2/database';
import { FirebaseProvider } from '../firebase/firebase';
import { Sale } from '../../models/sale.model';
import { PopUpProvider } from '../../providers/pop-up/pop-up'
/*
  Generated class for the SaleProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class SaleProvider extends FirebaseProvider {

  protected __path = 'sale';
    constructor(public firebase: AngularFireDatabase, public popupProvider : PopUpProvider) {
      super(firebase,popupProvider);
    }

    save(sale: Sale, key ?: string){
      if (key) {
        return this.push(sale,key);
      }
      else{
        return this.push(sale);
      }
    }

    customPath(path:string){
      this.setPath(path)
    }

    fetch(key: string){
      return this.getOneById(key);
    }

    fetcAll(){
      return this.getAll();
    }

    remove(key: string){
      return this.delete(key);
    }

}
