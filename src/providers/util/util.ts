import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';


import { Broker } from '../../models/broker.model';
import { Cooperative } from '../../models/cooperative.model';
import { CooperativeProvider } from '../../providers/cooperative/cooperative';
import { BrokerProvider } from '../../providers/broker/broker';
import { BrokerRequestProvider } from '../../providers/broker-request/broker-request';

import * as moment from 'moment';

/*
  Generated class for the UtilProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class UtilProvider {

	selected :any=[];

	constructor(public cooperativeProvider: CooperativeProvider, public brokerProvider: BrokerProvider, public requestProvider: BrokerRequestProvider) {
	    
	}

	purcentage(montant?:number, purcent?:number){
	    //montant(1- (purcent/100))
	    return (montant * (1 - ( purcent / 100 ) ) );
	}

	purcentageAgent(montant?:number, purcent?:number){
	    //montant((purcent/100))
	    return (montant * ( purcent / 100 ) );
	}

	is_broker(customer$){
		let selected:string[] = [];
	    this.brokerProvider.fetch(customer$.customer$).then((broker: Broker)=>{
	    	if (broker) {
	    		if (selected.indexOf(customer$.customer$) === -1) {
	    			selected.push(customer$.customer$);
	    		}
	    	}
	    });
	    if (selected) {
	    	return selected;
	    }
	}

	is_broker_C(customer$){
		let selected: string[] = [];

  		let customer = customer$.customer$;
  		
  		
		this.cooperativeProvider.fetcAll().subscribe((cooperatives: Cooperative)=>{
            for(let key in cooperatives){
                this.requestProvider.customPath(`cooperative/${key}/broker_request`);
                this.requestProvider.fetch(customer).then((brok: Broker)=>{
	                if (brok) {
	                    if(selected.indexOf(customer) === -1) {
					      selected.push(customer);
					    }
	                }
                })
            }
        });

		return selected;
		
	}


	
  is_broker_CVie(customer$):Promise<any>{
      let customer = customer$;
      let Coope:string[] = [];
     return new Promise(resolve => {
    this.cooperativeProvider.fetcAll().subscribe((cooperatives: Cooperative)=>{
            for(let key in cooperatives){ 
              let cooperative = cooperatives[key]['broker_request'];
              if (cooperative) {
                 for(let k in cooperative){
                   if (k===customer) {
                     Coope.push(key)
                   }
               }
              }
            }
          resolve(Coope);
        });
     });
  }


	differenceOfDate(first_date,last_date){
		var a = moment(first_date, 'YYYY-MM-DD');
	    var b = moment(last_date, 'YYYY-MM-DD');
	    let days = b.diff(a, 'days');
	    return days;
	}

}