import { Injectable } from '@angular/core';
import { AngularFireDatabase } from 'angularfire2/database';
import { FirebaseProvider } from '../firebase/firebase';
import { PopUpProvider } from '../../providers/pop-up/pop-up'

@Injectable()
export class TripProvider extends FirebaseProvider {

  protected __path = 'trip';
  constructor(public firebase: AngularFireDatabase, public popupProvider : PopUpProvider) {
    super(firebase,popupProvider);
  }

  customPath(path:string){
    this.setPath(path)
  }

  fetch(key: string){
    return this.getOneById(key);
  }

  fetcAll(){
    return this.getAll();
  }

}
