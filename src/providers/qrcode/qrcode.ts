import { Injectable } from '@angular/core';
import { AngularFireDatabase } from 'angularfire2/database';
import { FirebaseProvider } from '../firebase/firebase';
import { PopUpProvider } from '../../providers/pop-up/pop-up';
import { QRCode } from '../../models/qrcode.model'

/*
  Generated class for the QrcodeProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class QrcodeProvider extends FirebaseProvider {

  protected __path = 'qrcode';

  constructor(public firebase: AngularFireDatabase, public popupProvider : PopUpProvider) {
    super(firebase,popupProvider);
  }

  save(qrcode: QRCode, key ?: string){
    if (key) {
      return this.push(qrcode,key);
    }
    else{
      return this.push(qrcode);
    }
  }

  fetch(key: string){
    return this.getOneById(key);
  }

  fetcAll(){
    return this.getAll();
  }

  remove(key: string){
    return this.delete(key);
  }

}
