import { Injectable } from '@angular/core';
import { AngularFireDatabase } from 'angularfire2/database';
import { FirebaseProvider } from '../firebase/firebase';
import { BroKerRequest } from '../../models/broker-request.model';
import { PopUpProvider } from '../../providers/pop-up/pop-up'

/*
  Generated class for the BrokerRequestProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class BrokerRequestProvider extends FirebaseProvider {

  protected __path = 'broker_request';
    constructor(public firebase: AngularFireDatabase, public popupProvider : PopUpProvider) {
      super(firebase,popupProvider);
    }

    save(request: BroKerRequest, key ?: string){
      if (key) {
        return this.push(request,key);
      }
      else{
        return this.push(request);
      }
    }

    customPath(path:string){
      this.setPath(path)
    }

    fetch(key: string){
      return this.getOneById(key);
    }

    fetcAll(){
      return this.getAll();
    }

}
