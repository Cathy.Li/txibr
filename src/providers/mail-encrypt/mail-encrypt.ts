import { Injectable } from '@angular/core';
import { Md5 } from 'ts-md5/dist/md5';
import { ModalController } from 'ionic-angular';
import { RequestMailPage } from '../../pages/request-mail/request-mail';
/*
  Generated class for the MailEncryptProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class MailEncryptProvider {


  constructor(private modalCtrl : ModalController) {
  }


  mailEncrypt(mail?: string){
    return new Promise((resolve)=>{
      if(mail){
        let hashKey = Md5.hashStr(mail);
        resolve(String(hashKey));
      }else{
        let modal = this.modalCtrl.create(RequestMailPage, null, {cssClass:'pricebreakup' });
        modal.onDidDismiss((mail) => {
          if (mail) {
            let hashKey = Md5.hashStr(mail);
            resolve(String(hashKey));
          }
        });
        modal.present();
      }
    });
  }

}
