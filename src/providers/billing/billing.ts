import { Injectable } from '@angular/core';
import { AngularFireDatabase } from 'angularfire2/database';
import { FirebaseProvider } from '../firebase/firebase';
import { Billing } from '../../models/billing.model';
import { PopUpProvider } from '../../providers/pop-up/pop-up'

/*
  Generated class for the BillingProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
  */
  @Injectable()
  export class BillingProvider extends FirebaseProvider {

    protected __path = 'billing';
    constructor(public firebase: AngularFireDatabase, public popupProvider : PopUpProvider) {
      super(firebase,popupProvider);
    }

    customPath(path:string){
      this.setPath(path)
    }
    
    save(billing: Billing, key ?: string){
      if (key) {
        return this.push(billing,key);
      }
      else{
        return this.push(billing);
      }
    }

    fetch(key: string){
      return this.getOneById(key);
    }

    fetcAll(){
      return this.getAll();
    }

    remove(key: string){
      return this.delete(key);
    }

  }
