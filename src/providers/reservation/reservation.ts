import { Injectable } from '@angular/core';
import { AngularFireDatabase } from 'angularfire2/database';
import { FirebaseProvider } from '../firebase/firebase';
import { Reservation } from '../../models/reservation.model';
import { PopUpProvider } from '../../providers/pop-up/pop-up'

@Injectable()
export class ReservationProvider extends FirebaseProvider {

  protected __path = 'reservation';
  constructor(public firebase: AngularFireDatabase, public popupProvider : PopUpProvider) {
    super(firebase,popupProvider);
  }

  save(reservation: Reservation, key ?: string){
    if (key) {
      return this.push(reservation,key);
    }
    else{
      return this.push(reservation);
    }
  }

  customPath(path:string){
    this.setPath(path)
  }

  fetch(key: string){
    return this.getOneById(key);
  }

  fetcAll(){
    return this.getAll();
  }

  remove(key: string){
    return this.delete(key);
  }

}
