import { Injectable } from '@angular/core';
import { AngularFireDatabase } from 'angularfire2/database';
import { FirebaseProvider } from '../firebase/firebase';
import { PopUpProvider } from '../../providers/pop-up/pop-up';

/*
  Generated class for the PlanningProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class PlanningProvider extends FirebaseProvider {

  protected __path = 'planning';
  //protected __path = 'voyage';
  
  constructor(public firebase: AngularFireDatabase, public popupProvider : PopUpProvider) {
    super(firebase,popupProvider);
  }
  
  customPath(path:string){
    this.setPath(path)
  }
 
  fetch(key: string){
  	return this.getOneById(key);
  }

  fetcAll(){
  	return this.getAll();
  }

  save(planning: any, key ?: string){
    if (key) {
      return this.push(planning,key);
    }
    else{
      return this.push(planning);
    }
  }

}
