
import { Injectable } from '@angular/core';
import { AngularFireDatabase } from 'angularfire2/database';
import 'firebase/storage';
import * as firebase from 'firebase';
import { PopUpProvider } from '../../providers/pop-up/pop-up';


@Injectable()
export abstract class FirebaseProvider {

    //entity path in firebase database
    protected __path: string;
    constructor(public firebase: AngularFireDatabase, public popupProvider: PopUpProvider) {
      
    }

    getOne(id: string){
        return this.firebase.object(`${this.__path}/${id}`).valueChanges();  
    }

    getOneById(id : string){
      return new Promise ((resolve)=>{
        this.firebase.object(`${this.__path}/${id}`).valueChanges().subscribe((object)=>{
          resolve(object);
        });
      });

    }

    getAll(){
      return this.firebase.object(this.__path).valueChanges();
    }

    push(object: any, key ?: string){
      if(key){
        return this.firebase.list(this.__path).update(key,object);
      }
      else{
        return this.firebase.list(this.__path).push(object).key;
      }
    }

    upload(file){

      return new Promise((resolve)=>{

        this.popupProvider.showLoading('0 %');

        let id = '' + Math.floor(Math.random() * 1000000);

        let _path = this.getPath();

        let path = `${_path}/${id}`;

        let ref = firebase.storage().ref(path);

        //let task = ref.put(file);

        let task = ref.putString(file, 'base64', {contentType: 'image/png'})


        task.on('state_changed',
          (snapshot) => {
            const snap = snapshot as firebase.storage.UploadTaskSnapshot;

            let percentage = Math.round((snap.bytesTransferred / snap.totalBytes) * 100);

            this.popupProvider.setLoadingContent(percentage + ' %');
          },
          (error) => {
            alert (JSON.stringify(error));
          },
          () => {
            ref.getDownloadURL().then((url)=>{
              this.popupProvider.dismissLoading();
              resolve(url);
            })
          }
          )

      })
    }

    setPath(customPath: string){
      this.__path = customPath;
    }

    getPath(){
      return this.__path;
    }

    delete(key:string){
      this.firebase.list(this.__path).remove(key);
    }

    deleteFile(url: string){
      let storageRef = firebase.storage().refFromURL(url);
      storageRef.delete();
    }

  }
