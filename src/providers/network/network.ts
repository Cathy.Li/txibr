import { Injectable } from '@angular/core';
import { PopUpProvider } from '../pop-up/pop-up';
import { Network } from '@ionic-native/network';
import { Diagnostic } from '@ionic-native/diagnostic';

/*
  Generated class for the NetworkProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
  */
  @Injectable()
  export class NetworkProvider {

  	constructor(
      private popupProvider: PopUpProvider,
      private network: Network,
      private diagnostic: Diagnostic
    ) {

  	}

  	checkConnection(){
      this.network.onDisconnect().subscribe(()=>{
        this.switchSettings('network');
      })
  	}

  	switchSettings(type: string){

      /*var options = {
          okText: 'Parametre',
          canceltext: 'Annuler'
      }*/

  		switch (type) {
  			case "network":
  			this.popupProvider.presentConfirm('Veuillez rétablir votre connexion','Erreur de connexion').then((parameter)=>{
          this.diagnostic.switchToWirelessSettings();
        }, (cancel)=>{});
  			break;

        case "bluetooth":
          this.popupProvider.presentConfirm('Veuillez rétablir votre bluetooth','Bluetooth désactivé').then((parameter)=>{
            this.diagnostic.switchToBluetoothSettings();
          }, (cancel)=>{})
        break;  			
  		}
  	}

  	settings(){

  	}

  	exit(){

    }

    bluetoothSettings(){
      this.switchSettings('bluetooth');
    }




  }
