import { Injectable } from '@angular/core';
import { AngularFireDatabase } from 'angularfire2/database';
import { FirebaseProvider } from '../firebase/firebase';
import { Worker } from '../../models/worker.model';
import { PopUpProvider } from '../../providers/pop-up/pop-up'

/*
  Generated class for the WorkerProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class WorkerProvider extends FirebaseProvider {

    protected __path = 'worker';
    constructor(public firebase: AngularFireDatabase, public popupProvider : PopUpProvider) {
      super(firebase,popupProvider);
    }

    fetchOne(id: string){
      return this.getOne(id);
    }

    save(worker: Worker, key ?: string){
      if (key) {
        return this.push(worker,key);
      }
      else{
        return this.push(worker);
      }
    }

    customPath(path:string){
      this.setPath(path)
    }

    fetch(key: string){
      return this.getOneById(key);
    }

    fetcAll(){
      return this.getAll();
    }

}
