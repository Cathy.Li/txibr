import { AlertController, ToastController, Alert, Toast, LoadingController, Loading } from 'ionic-angular';
import { Injectable } from '@angular/core';

@Injectable()
export class PopUpProvider {

  alert: Alert;
  toast: Toast;
  loading: Loading;

  constructor(private alertCtrl: AlertController, private toastCtrl: ToastController, private loadingCtrl: LoadingController) {

  }

  presentConfirm(message?:string, title?:string){
      return new Promise((resolve, reject)=>{
        this.alert = this.alertCtrl.create({
          title: title || 'Confirmation',
          message: message || 'Voulez vous effectuer cette action ?',
          buttons: [
          {
            text: 'Annuler',
            role: 'cancel',
            handler: () =>{
              reject(false)
            }
          },
          {
            text: 'OK',
            handler: () => {
              resolve(true)
            }
          }
          ],
          cssClass: 'tb-alert'
        })

        this.alert.present();
      })
    }

  presentAlert(message:string, title?:string){
    this.alert = this.alertCtrl.create({
      title: title || 'Notification',
      message: message,
      buttons: ['OK'],
      cssClass: 'tb-alert'
    });
    this.alert.present();
  }

  PresentToast(text: string, success?, duration?, position?, closeButton?, btnText?){
    this.dismissToast();
    if (!this.toast) {
        this.toast = this.toastCtrl.create({
        message: text,
        duration: duration || closeButton ? null : 5000,
        position: position || 'top',
        showCloseButton: closeButton || false,
        closeButtonText: btnText || 'OK',
        cssClass: success ? 'successToast' : '',
      });
      this.toast.present();

    }
  }

  dismissToast(){
    if (this.toast) {
      this.toast.dismissAll();
      this.toast = null;
    }
  }

  showLoading(content?: string) {
    if(!this.loading){
      this.loading = this.loadingCtrl.create({
        content: content || 'Chargement ...',
        dismissOnPageChange: false,
        enableBackdropDismiss: true
      });
      this.loading.present();
    }
  }

  dismissLoading(){
    if(this.loading){
      this.loading.dismiss();
      this.loading = null;
    }
  }

  setLoadingContent(content:string){
    if (this.loading) {
      this.loading.data.content = content;
    }
  }



}
