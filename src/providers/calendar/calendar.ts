import { Injectable } from '@angular/core';
import { CalendarModal, CalendarModalOptions, CalendarResult, DayConfig } from "ion2-calendar";
import { ModalController } from 'ionic-angular';

/*
  Generated class for the CalendarProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
  */
  @Injectable()
  export class CalendarProvider {

  	constructor(private modalCtrl: ModalController) {

  	}

  	open(disableWeeks?:any,dateMarked?:any){
  		return new Promise((resolve)=>{

        let _daysConfig: DayConfig[] = [];
        if (dateMarked) {
         _daysConfig.push({
            date: dateMarked,
            subTitle: `réservation modifié`
          })
        }
       

  			const options: CalendarModalOptions = {
          defaultDate:dateMarked||'',
  				disableWeeks: disableWeeks || [],
  				weekdays : ['Dim','Lun','Mar','Mer','Jeu','Ven','Sam'],
  				title: 'Date de départ',
  				color: 'dark',
  				closeLabel: 'Annuler',
  				doneLabel: 'ok',
  				weekStart:1,
  				cssClass: 'taxibr-calendar',
          daysConfig: _daysConfig

  			};

  			let myCalendar =  this.modalCtrl.create(CalendarModal, {
  				options: options
  			});

  			myCalendar.present();

  			myCalendar.onDidDismiss((date: CalendarResult, type: string) => {

  				if (date) {
  					resolve(date)  ;				
  				}
  				else{
  					resolve(false);
  				}

  			});
  		});
  	}

  }
