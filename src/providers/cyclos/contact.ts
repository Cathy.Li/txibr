import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptionsArgs } from '@angular/http';
import { CyclosProvider } from './cyclos';
import { UserProfile } from '../../lib/models/user';
import { Session } from '../../lib/models/session';
import { Config } from '../../config/config';
import { AlertController } from 'ionic-angular';

@Injectable()
export class ContactProvider extends CyclosProvider {

	resource = 'contacts';
	session : Session
     constructor(protected http: Http,
      private alertCtrl: AlertController) {
  	super(http);
    }
	public action(action: string, user?: UserProfile) {
		let method: string;
		let url: string = 'self/' + this.resource;
		switch (action) {
			case "add":
				method = 'POST';
				url  += '/' + user.id;
				break;
			case "delete":
				method = 'DELETE';
				url  += '/' + user.id;
				break;
			case "get":
				method = 'GET';
				break;
		}
		return new Promise((resolve, reject) => {
			this.fetch(url, method).subscribe(
				(response) => {
					if (response.status == 200)
						resolve(response.json());
					else
							reject()
				}, (error) => {
					console.error(error);
					this.manageError(error);
					reject(error);
				})
		})
	}
    getContact(session : Session ) {
        let header: Headers = this.tokenHeader(new Headers,session.sessionToken)
        console.log(header)
        let reqOptArg: RequestOptionsArgs = this.defaultRequestOptionArgs(header)
        console.log(reqOptArg)
        return new Promise((resolve,reject) => {
        this.http.get(Config.endpoints.cyclos + "self/contacts", reqOptArg).toPromise().then((response :any) => {
	        // this.loading.dismiss()
	         resolve(response._body)
	        }).catch((error) => {
            // this.loading.dismiss()
            reject(error)
            })
        })
    }

    // setContact( session: Session , keyword: string) {
    //     return new Promise((resolve, reject) => {
    //     let header: Headers = this.tokenHeader(new Headers,session.sessionToken)
    //     let reqOptArg: RequestOptionsArgs = this.defaultRequestOptionArgs(header)
    //     this.http.post(Config.endpoints.cyclos + "self/contacts/" +keyword ,reqOptArg).toPromise().then((response :any) => {
    //     resolve(response._body)
    //         }).catch((error) => {
    //         this.manageError(error);
    //         reject(error)
    //         })
    //    })
    // }


     setContact( session: Session ,key:string) {
     return new Promise((resolve, reject) => {
      let header: Headers = this.tokenHeader(new Headers,session.sessionToken)
      let reqOptArg: RequestOptionsArgs = this.defaultRequestOptionArgs(header)
      this.http.post(Config.endpoints.cyclos + "self/contacts/" +key , "", reqOptArg).toPromise().then((response :any) => {
        resolve(response._body)
      }).catch((error) => {
        this.manageError(error);
        reject(error)
      })
    })
  }
    protected manageError(error) {
    // let eBody = error.json();
    if (error.status == 200) {
      this.presentAlert("Erreur de transfert")
    } 
  }

   protected presentAlert(message: string) {
    this.alertCtrl.create({
      title: "Attention",
      message: message,
      buttons: [{
        text: 'OK'
      }]
    }).present()
  }
}
