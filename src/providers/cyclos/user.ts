import { Http, Headers, RequestOptionsArgs } from '@angular/http';
import { Injectable } from '@angular/core';
import { CyclosProvider } from './cyclos';
import { Observable } from 'rxjs';
import { Credential } from '../../lib/credential';
import { Config } from '../../config/config';
import { Session } from '../../lib/models/session';
import { LoadingController, Loading } from 'ionic-angular';



@Injectable()
export class UserProvider extends CyclosProvider {
    session :Session
    loading:Loading
    key : string ;

    constructor(public http: Http,public loadingCtrl: LoadingController) {
      super(http);
    }

    getUser(keywords: string) {
        return new Promise((resolve, reject) => {
            let params = {keywords: keywords};
            this.fetch(this.resource, 'GET', params).subscribe((response) => {
                console.log(response);
            }, (error) => {
                console.error(error);
            });
            
        })
    }
 
    getUsers() {
        return new Promise((resolve, reject) => {
        this.fetch(this.resource, 'GET').subscribe((response) => {
                console.log(response);
            }, (error) => {
                console.error(error);
            });
        })
    }
    getUsersbycontact(session : Session, keywords?:string) {
        let header: Headers = this.tokenHeader(new Headers,session.sessionToken)
        console.log(header)
        let reqOptArg: RequestOptionsArgs = this.defaultRequestOptionArgs(header)
        console.log(reqOptArg)
        return new Promise((resolve,reject) => {
            this.http.get(Config.endpoints.cyclos + "users?keywords=" +keywords, reqOptArg).toPromise().then((response :any) => {
            resolve(response._body)
            }).catch((error) => {
            reject(error)
            })
       })
    }
}
