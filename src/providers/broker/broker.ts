import { Injectable } from '@angular/core';
import { AngularFireDatabase } from 'angularfire2/database';
import { FirebaseProvider } from '../firebase/firebase';
import { Broker } from '../../models/broker.model';
import { PopUpProvider } from '../../providers/pop-up/pop-up'

/*
  Generated class for the BrokerProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class BrokerProvider extends FirebaseProvider {

   	protected __path = 'broker';
    constructor(public firebase: AngularFireDatabase, public popupProvider : PopUpProvider) {
      super(firebase,popupProvider);
    }

     save(broker: Broker, key ?: string){
      if (key) {
        return this.push(broker,key);
      }
      else{
        return this.push(broker);
      }
    }

    fetch(key: string){
      return this.getOneById(key);
    }

}
