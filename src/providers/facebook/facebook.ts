import { Injectable } from '@angular/core';
import { Facebook } from '@ionic-native/facebook';
import { Profile } from '../../models/profile.model';
import { MailEncryptProvider } from '../mail-encrypt/mail-encrypt';

@Injectable()
export class FacebookProvider {


  connected: boolean = false;

  constructor(private facebook: Facebook, public mailEncryptProvider: MailEncryptProvider){

  }

  login(){
    return new Promise((resolve)=>{
      this.facebook.login(['email', 'public_profile']).then(()=>{
        resolve(true);
      })
    })
  }

  logout() {
    return new Promise((resolve)=>{
      this.facebook.logout().then(() => {
        resolve(true);
      })
    })
  }

  getUser(){
    return new Promise((resolve)=>{
      this.facebook.api('me?fields=id,email,name,picture.width(720).height(720).as(picture_large)', []).then((profile)=>{

        let mailEncrypt: any;
          this.mailEncryptProvider.mailEncrypt(profile['email']).then((data)=>{
            mailEncrypt = data;
          });

        let user = {
            id: profile['id'],
            email: profile['email'],
            name: profile['name'],
            picture: profile['picture_large']['data']['url'],
          };

          resolve(new Profile(user));

      });
    });
  }

  status(){
    return new Promise((resolve)=>{
      this.facebook.getLoginStatus().then((reponse)=>{

        if(reponse.status === 'connected'){
          this.connected = true;
        }

        resolve(this.connected)

      })
    })
  }

}
