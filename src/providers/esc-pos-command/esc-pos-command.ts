import { Injectable } from '@angular/core';
// import * as EscPosEncoder  from 'esc-pos-encoder';

/*
  Generated class for the EscPosCommandProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
  */
  @Injectable()
  export class EscPosCommandProvider {

  	// encoder: EscPosEncoder = new EscPosEncoder();
  	constructor() {

  	}

  	barcodeEncoder(code: string){
  		var chr = function(number) {
  			return String.fromCharCode(number);
  		}
  		var barcode = '\x1D' + 'h' + chr(80) + '\x1D' + 'w' + chr(2) + '\x1D' + 'f' + chr(0) + '\x1D' + 'k' + chr(73) + chr(code.length) + code + chr(0); 
  		return barcode; 
  	}

  	qrcodeEncoder(code: string){
  		/*var chr = function(number) {
  			return String.fromCharCode(number);
  		}*/
      var qr = 'a';
      var dots = '\x08';
      var size1 = String.fromCharCode(qr.length + 3);
      var size0 = '\x00'; 
      var qrcode = '\x1D' + '\x28' + '\x6B' + '\x04' + '\x00' + '\x31' + '\x41' + '\x32' + '\x00' + '\x1D' + '\x28' + '\x6B' + '\x03' + '\x00' + '\x31' + '\x43' + dots + '\x1D' + '\x28' + '\x6B' + '\x03' + '\x00' + '\x31' + '\x45' + '\x30' + '\x1D' + '\x28' + '\x6B' + size1 + size0 + '\x31' + '\x50' + '\x30' + qr + '\x1D' + '\x28' + '\x6B' + '\x03' + '\x00' + '\x31' + '\x51' +'\x30' + '\x1D' + '\x28' + '\x6B' + '\x03' + '\x00' + '\x31' + '\x52' +'\x30';
      return qrcode;
    }

  }
