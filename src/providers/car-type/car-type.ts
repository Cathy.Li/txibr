import { Injectable } from '@angular/core';
import { AngularFireDatabase } from 'angularfire2/database';
import { FirebaseProvider } from '../firebase/firebase';
import { PopUpProvider } from '../../providers/pop-up/pop-up'


  @Injectable()
  export class CarTypeProvider extends FirebaseProvider {

  	protected __path = 'cartype';
    constructor(public firebase: AngularFireDatabase, public popupProvider : PopUpProvider) {
      super(firebase,popupProvider);
    }

    fetch(key: string){
      return this.getOneById(key);
    }

    fetcAll(){
      return this.getAll();
    }

  }
