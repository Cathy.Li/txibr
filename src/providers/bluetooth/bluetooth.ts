import { BluetoothSerial } from '@ionic-native/bluetooth-serial';
import { Injectable } from '@angular/core';
import { Refresher } from 'ionic-angular';
import { PopUpProvider } from '../pop-up/pop-up';
import { NetworkProvider } from '../network/network'

/*
  Generated class for the BluetoothProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
  */
  @Injectable()
  export class BluetoothProvider {

    devices = [];
    constructor(
      private bluetooth: BluetoothSerial,
      private popup: PopUpProvider,
      private networkProvider: NetworkProvider
      ) {
    }

    search(){
      return new Promise((resolve, reject)=>{
        this.enabled().then((isEnabled)=>{
          this.popup.showLoading();
          this.devices = [];
          this.bluetooth.list().then((pairedDevices)=>{
            if (pairedDevices) {
              this.devices = pairedDevices;
              this.popup.dismissLoading();
              resolve(this.devices);
            }
            else{
              this.popup.presentAlert('Aucun appareil trouvé');
              reject();
            }
          }).catch((error)=>{
            this.popup.presentAlert('Bluetooth non disponible sur cette plateforme');
            reject();
          });
        });
      });
    }

    enabled(){
      return new Promise((resolve)=>{
        this.bluetooth.isEnabled().then((success)=>{
          resolve(true)
        }, (fail)=>{
          this.networkProvider.bluetoothSettings();
          resolve(false);
        });
      });
    }

    searchUnpaired(){
      return new Promise((resolve, reject)=>{
        this.bluetooth.isEnabled().then((success)=>{
          this.popup.showLoading();
          this.bluetooth.discoverUnpaired().then((devices)=>{
            if (devices) {
              this.devices = [];
              this.popup.dismissLoading();
              this.devices = devices;
              resolve(this.devices);
            }
            else{
              this.popup.presentAlert('Aucun appareil trouvé');
              reject();
            }
          }).catch((error)=>{
            this.popup.presentAlert('Bluetooth non disponible sur cette plateforme');
            reject();
          });
        }, (fail) => {
          //this.popup.presentAlert('Bluetooth n\'est pas disponible ou est désactivé');
          this.networkProvider.bluetoothSettings();
          reject();
        });
      });
    }

    connect(address){
      return new Promise((resolve, reject)=>{
        this.bluetooth.connect(address).subscribe((connected) => {
          this.bluetooth.clear().then(()=>{
            resolve(connected)
          })
        }, (fail) => {
          this.popup.presentAlert('Impossible de se connecter a cette appareil');
          reject();
        })
      })
    }

    disconnect(){
      return new Promise((resolve, reject)=>{
        if (this.bluetooth.isConnected) {
          this.bluetooth.disconnect().then((disconnected)=>{
            resolve(disconnected);
          })
        }
      })
    }

    write(data){
      return this.bluetooth.write(data);
    }

    refresh(refresher: Refresher){
      return new Promise((resolve, reject)=>{
        if (refresher) {
          this.searchUnpaired().then((success: Array <Object>)=>{
            this.devices = [];
            this.devices = success;
            refresher.complete();
            resolve(this.devices);
          }, (fail)=>{
            reject();
          })
        }
      })
    }

  }
