import { Injectable } from '@angular/core';
import {AngularFireAuth} from 'angularfire2/auth';
import {Observable} from "rxjs/Observable";
import {auth} from 'firebase';
import { Profile } from '../../models/profile.model';
import {Platform} from 'ionic-angular';

/*
  Generated class for the AuthProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class AuthFbProvider {

  connected: boolean = false;

  constructor(private afAuth: AngularFireAuth, public platform : Platform) {
  }
  
  loginWithFacebook() {
    return Observable.create(observer => {
        this.afAuth.auth
          .signInWithPopup(new auth.FacebookAuthProvider())
          .then((res) => {
            observer.next(res);
          }).catch(error => {
          observer.error(error);
       })
    });
  }


  getUser(){
    return new Promise((resolve)=>{
      let profile = this.afAuth.auth.currentUser;
      console.log(profile);
      let picture = "https://graph.facebook.com/" + profile.providerData[0]['uid'] + "/picture?type=large";
      let user = {
            id: profile.providerData[0]['uid'],
            email: profile['email'],
            name: profile['displayName'],
            picture: picture
      };
      //console.log(user);
      resolve(new Profile(user));
    });
  }



}
