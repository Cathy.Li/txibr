export interface NFCToken {
	type?: string,
	token?: string,
	cyclosChallenge?: string
}