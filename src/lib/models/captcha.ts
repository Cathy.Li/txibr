export interface Captcha {
  challenge?: string,
  response?: string
}