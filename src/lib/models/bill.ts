export interface Bill {
	amount: string, 
	bill: string,
	cles_user: string,
	description: string,
	ref_client: string
}