export interface BankingSlip {
  idUser?:string,
  20000?:string,
  10000?:string,
  5000?:string,
  1000?:string,
  500?:string,
  200?:string,
  100?:string,
  50?:string,
  20?:string,
  10?:string,
}
