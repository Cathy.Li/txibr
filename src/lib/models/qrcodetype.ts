export interface QRCodeType  {
	bill: boolean,
	contact: boolean,
	slip: boolean,
	safety: boolean,
	register: boolean
}