export interface Transfer {
	creditedAccountId: string
	debitedAccountId: string
	amount: number,
	wording: string
}