import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
import { FIREBASE_CONFIG,FINPAY_FIREBASE_CONFIG } from './app.firebase.config';
import { AngularFireModule } from 'angularfire2';
import { AngularFireDatabaseModule } from 'angularfire2/database';
import { MyApp } from './app.component';
import { CarProvider } from '../providers/car/car';
import { CarTypeProvider } from '../providers/car-type/car-type';
import { CooperativeProvider } from '../providers/cooperative/cooperative';
import { FacebookProvider } from '../providers/facebook/facebook';
import { PopUpProvider } from '../providers/pop-up/pop-up';
import { PrinterProvider } from '../providers/printer/printer';
import { ReservationProvider } from '../providers/reservation/reservation';
import { StationProvider } from '../providers/station/station';
import { TripProvider } from '../providers/trip/trip';
import { VoyageProvider } from '../providers/voyage/voyage';
import { Facebook } from '@ionic-native/facebook';
import { AvailableBookingPage } from '../pages/available-booking/available-booking';
import { AvailableCarPage } from '../pages/available-car/available-car';
import { BookingDetailsPage } from '../pages/booking-details/booking-details';
import { ConnectedPage } from '../pages/connected/connected';
import { FinpayPaiementPage } from '../pages/finpay-paiement/finpay-paiement';
import { LoginPage } from '../pages/login/login';
import { MyBookingsPage } from '../pages/my-bookings/my-bookings';
import { PaiementMethodPage } from '../pages/paiement-method/paiement-method';
import { QuickBookingPage } from '../pages/quick-booking/quick-booking';
import { SeatPickerPage } from '../pages/seat-picker/seat-picker';
import { SplashPage, } from '../pages/splash/splash';
import { NgxQRCodeModule } from 'ngx-qrcode2';
import { QrScannerProvider } from '../providers/qr-scanner/qr-scanner';
import { QRScanner } from '@ionic-native/qr-scanner';
import { QrScannerPage } from '../pages/qr-scanner/qr-scanner';
import { Printer } from '@ionic-native/printer';
import { BluetoothSerial } from '@ionic-native/bluetooth-serial';
import { PrinterWidgetPage } from '../pages/printer-widget/printer-widget';
import { DatePicker } from '@ionic-native/date-picker';
import { AvailableBluetoothPrinterPage } from '../pages/available-bluetooth-printer/available-bluetooth-printer'
import { PlanningProvider } from '../providers/planning/planning';
import { TrajectProvider } from '../providers/traject/traject';
import { BookingClassProvider } from '../providers/booking-class/booking-class';
import { SearchPipe } from '../pipes/search/search';
import { PlaceProvider } from '../providers/place/place';
import { EventProvider } from '../providers/event/event';
import { CalendarModule } from "ion2-calendar";
import { CalendarProvider } from '../providers/calendar/calendar';
import { BillingProvider } from '../providers/billing/billing';
import { AvailableBookingPipe } from '../pipes/available-booking/available-booking';
import { CarPipe } from '../pipes/car/car'
import { NetworkProvider } from '../providers/network/network';
import { BluetoothProvider } from '../providers/bluetooth/bluetooth';
import { EscPosCommandProvider } from '../providers/esc-pos-command/esc-pos-command';
import { QrcodeProvider } from '../providers/qrcode/qrcode';
import { BrokerRegisterPage } from '../pages/broker-register/broker-register'
import { BrokerProvider } from '../providers/broker/broker';
import { SaleProvider } from '../providers/sale/sale';
import { MySalesPage } from '../pages/my-sales/my-sales';
import { BrokerMenuPage } from '../pages/broker-menu/broker-menu';
import { CooperativeBrokerPage } from '../pages/cooperative-broker/cooperative-broker'
import { BrokerRequestProvider } from '../providers/broker-request/broker-request';
import { WorkerProvider } from '../providers/worker/worker';
import { MailEncryptProvider } from '../providers/mail-encrypt/mail-encrypt';
import { RequestMailPage } from '../pages/request-mail/request-mail';
import { Network } from '@ionic-native/network';
import { Diagnostic } from '@ionic-native/diagnostic';
import { GoogleMaps } from '@ionic-native/google-maps';
import { GoogleMapsComponent } from '../components/google-maps/google-maps';
import { MapPage } from '../pages/map/map';
//jean soa
 
import { Camera } from '@ionic-native/camera';
import { StatusPaiementPage } from '../pages/status-paiement/status-paiement';
import { AuthFbProvider } from '../providers/auth/auth';
import { LoginFinpayPage } from '../pages/login-finpay/login-finpay';
import { ConfirmPasswordFinpayPage } from '../pages/confirm-password-finpay/confirm-password-finpay';
import { CyclosProvider } from '../providers/cyclos/cyclos';
import { OrchidProvider } from '../providers/orchid/orchid';
import { SfProvider } from '../providers/sf/sf';
import { AuthProvider } from '../providers/cyclos/auth';
import { StorageProvider } from '../providers/storage/storage';
import { PaymentProvider } from '../providers/cyclos/payment';
import { HttpModule } from '@angular/http';
import { HttpClientModule } from '@angular/common/http';
import { IonicStorageModule } from '@ionic/storage';
import { AccountProvider } from '../providers/cyclos/account';

import { RegisterFinpayPage } from '../pages/register-finpay/register-finpay';
import { PictUploadPage } from '../pages/pict-upload/pict-upload';
import { FinpayFirebaseProvider } from '../providers/finpay-firebase/finpay-firebase';
import { BrMaskerModule } from 'brmasker-ionic-3';
import { InfoLoginComponent } from '../components/info-login/info-login';
import { PictComponent } from '../components/pict/pict';
import { CameraProvider } from '../providers/camera/camera';
import { AccountDetailPage } from '../pages/account-detail/account-detail';
import { UserProvider } from '../providers/cyclos/user';
import { CooperativeBrokerRegisterPage } from '../pages/cooperative-broker-register/cooperative-broker-register';
import { UtilProvider } from '../providers/util/util';


@NgModule({
  declarations: [
    MyApp,
    AvailableBookingPage,
    AvailableCarPage,
    BookingDetailsPage,
    ConnectedPage,
    FinpayPaiementPage,
    LoginPage,
    MyBookingsPage,
    PaiementMethodPage,
    QuickBookingPage,
    SeatPickerPage,
    SplashPage,
    QrScannerPage,
    PrinterWidgetPage,
    AvailableBluetoothPrinterPage,
    BrokerRegisterPage,
    BrokerMenuPage,
    MySalesPage,
    CooperativeBrokerPage,
    SearchPipe,
    AvailableBookingPipe,
    CarPipe,
    RequestMailPage,
    GoogleMapsComponent,
    MapPage,
    StatusPaiementPage,
    LoginFinpayPage,
    ConfirmPasswordFinpayPage,
    RegisterFinpayPage,
    PictUploadPage,
    InfoLoginComponent,
    PictComponent,
    AccountDetailPage,
    CooperativeBrokerRegisterPage
  ],
  imports: [
    BrowserModule,
    BrMaskerModule,
    AngularFireModule.initializeApp(FIREBASE_CONFIG),
    //AngularFireModule.initializeApp(FINPAY_FIREBASE_CONFIG,'finpay'),
    AngularFireDatabaseModule,
    IonicModule.forRoot(MyApp),
    IonicStorageModule.forRoot(),
    NgxQRCodeModule,
    CalendarModule,
    HttpModule,
    HttpClientModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    AvailableBookingPage,
    AvailableCarPage,
    BookingDetailsPage,
    ConnectedPage,
    FinpayPaiementPage,
    LoginPage,
    MyBookingsPage,
    PaiementMethodPage,
    QuickBookingPage,
    SeatPickerPage,
    SplashPage,
    QrScannerPage,
    PrinterWidgetPage,
    AvailableBluetoothPrinterPage,
    BrokerRegisterPage,
    BrokerMenuPage,
    MySalesPage,
    CooperativeBrokerPage,
    RequestMailPage,
    GoogleMapsComponent,
    MapPage,
    StatusPaiementPage,
    LoginFinpayPage,
    ConfirmPasswordFinpayPage,
    RegisterFinpayPage,
    PictUploadPage,
    InfoLoginComponent,
    PictComponent,
    AccountDetailPage,
    CooperativeBrokerRegisterPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    Camera,
    CarProvider,
    CarTypeProvider,
    CooperativeProvider,
    Facebook,
    FacebookProvider,
    PopUpProvider,
    PrinterProvider,
    ReservationProvider,
    StationProvider,
    TripProvider,
    VoyageProvider,
    QRScanner,
    QrScannerProvider,
    Printer,
    BluetoothSerial,
    PrinterProvider,
    DatePicker,
    PlanningProvider,
    TrajectProvider,
    BookingClassProvider,
    PlaceProvider,
    EventProvider,
    CalendarProvider,
    BillingProvider,
    NetworkProvider,
    BluetoothProvider,
    EscPosCommandProvider,
    QrcodeProvider,
    BrokerProvider,
    SaleProvider,
    BrokerRequestProvider,
    WorkerProvider,
    MailEncryptProvider,
    Network,
    Diagnostic,
    GoogleMaps,
    AuthFbProvider,
    CyclosProvider,
    OrchidProvider,
    SfProvider,
    AuthProvider,
    StorageProvider,
    PaymentProvider,
    AccountProvider,
    FinpayFirebaseProvider,
    CameraProvider,
    UserProvider,
    UtilProvider
  ]
})
export class AppModule {}
