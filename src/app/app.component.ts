import { Component, ViewChild } from '@angular/core';
import { Nav, Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { ModalController } from 'ionic-angular';
import { FacebookProvider } from '../providers/facebook/facebook';
import { ConnectedPage } from '../pages/connected/connected';
import { SplashPage } from '../pages/splash/splash';
import { LoginPage } from '../pages/login/login';
import { QuickBookingPage } from '../pages/quick-booking/quick-booking';
import { MyBookingsPage } from '../pages/my-bookings/my-bookings';
import { EventProvider } from '../providers/event/event';
import { BrokerRegisterPage } from '../pages/broker-register/broker-register';
import { BrokerProvider } from '../providers/broker/broker';
import { Broker } from '../models/broker.model';
import { MySalesPage } from '../pages/my-sales/my-sales';
import { BrokerMenuPage } from '../pages/broker-menu/broker-menu';
import { NetworkProvider } from '../providers/network/network'


import { StatusPaiementPage } from '../pages/status-paiement/status-paiement';
import { PaiementMethodPage } from '../pages/paiement-method/paiement-method';
import { RegisterFinpayPage } from '../pages/register-finpay/register-finpay';
import { PictUploadPage } from '../pages/pict-upload/pict-upload';
import { LoginFinpayPage } from '../pages/login-finpay/login-finpay';


import { UtilProvider } from '../providers/util/util';

@Component({
  templateUrl: 'app.html'
})
export class MyApp {

  @ViewChild(Nav) nav: Nav;

  rootPage:any = LoginPage;
  // rootPage:any = MyBookingsPage;
  // rootPage:any = MySalesPage;
  pages: any = [];

 
  constructor(
    platform: Platform,
    statusBar: StatusBar,
    private modalCrtl: ModalController,
    private facebookProvider: FacebookProvider,
    private evenProvider: EventProvider,
    private brokerProvider: BrokerProvider,
    private netwotkProvider: NetworkProvider,
    private util: UtilProvider
    ) {

      this.pages = [
        {title: 'Profil', component: ConnectedPage, param:{}, icon:'contact'},
        {title: 'FinPAY', component: LoginFinpayPage, param:{}, icon:'settings'}
      ]

    platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.

      this.netwotkProvider.checkConnection();

      this.facebookProvider.status().then((reponse)=>{

        if (reponse) {
          this.rootPage = ConnectedPage;
        }
      })

      statusBar.styleDefault();
      let splash = modalCrtl.create(SplashPage) ;
      splash.present() ;
    });

    this.evenProvider.getEvent('customer$').then((customer$)=>{
      if (customer$) {
        this.pages.push({title: 'Réserver', component: QuickBookingPage, param: customer$, icon:'bus'});

          if (this.util.is_broker_C(customer$) || this.util.is_broker(customer$) ) {
            this.pages.push({title: 'Mes réservations', component: MyBookingsPage, param: customer$, icon:'list'});
            this.pages.push({title: 'Ventes', component: MySalesPage, param: customer$, icon:'logo-usd'});
          }else{
            this.pages.push({title: 'Mes réservations', component: MyBookingsPage, param: customer$, icon:'list'});
          }
        this.pages.push({title: 'Agent', component: BrokerMenuPage, param: customer$, icon:'person'});
      }
    })


  }

  onPage(page){
    this.nav.setRoot(page.component, page.param);
  }

  logout(){
    this.facebookProvider.logout().then(()=>{
      this.nav.setRoot(LoginPage)
    })
  }

}
