// export const FIREBASE_CONFIG = {
// 	apiKey: "AIzaSyB8BG36MalAlcQ7ye6WOtjZvLadaofOO0c",
// 	authDomain: "crud-ccdaa.firebaseapp.com",
// 	databaseURL: "https://crud-ccdaa.firebaseio.com",
// 	projectId: "crud-ccdaa",
// 	storageBucket: "crud-ccdaa.appspot.com",
// 	messagingSenderId: "566261345441"
// };

/*export const FIREBASE_CONFIG = {
	apiKey: "AIzaSyALSgQf83VX_78dwmPb9FvMzlp24ksF28g",
	authDomain: "axibr-3ab02.firebaseapp.com",
	databaseURL: "https://taxibr-3ab02.firebaseio.com",
	projectId: "axibr-3ab02",
	storageBucket: "taxibr-3ab02.appspot.com",
	messagingSenderId: "945529410580"
};*/

export const FIREBASE_CONFIG_PROD = {
    apiKey: "AIzaSyBe0g70xWI8AJIqP-78gI_hyY0gq-NOSLg",
    authDomain: "taxibr-6f31c.firebaseapp.com",
    databaseURL: "https://taxibr-6f31c.firebaseio.com",
    projectId: "taxibr-6f31c",
    storageBucket: "taxibr-6f31c.appspot.com",
};

export const FIREBASE_CONFIG = {
	apiKey: "AIzaSyCQkH013wZPncjk2b64goxb6sjUfsNf8yI",
    authDomain: "taxibr-bd321.firebaseapp.com",
    databaseURL: "https://taxibr-bd321.firebaseio.com",
    projectId: "taxibr-bd321",
    storageBucket: "taxibr-bd321.appspot.com",
    messagingSenderId: "647013876514"
};

export const FINPAY_FIREBASE_CONFIG = {
    apiKey: "AIzaSyCiDnobh4n9IJ_cXBE7_tpTXcRZKiHD628",
    authDomain: "fireapi-971aa.firebaseapp.com",
    databaseURL: "https://fireapi-971aa.firebaseio.com",
    projectId: "fireapi-971aa",
    storageBucket: "fireapi-971aa.appspot.com",
    messagingSenderId: "836147332864"
};

export const FIREBASE_CONFIG_FINPAY = {
  apiKey: "AIzaSyDtbDNr1seooUxW1TkUcJ9R0HsCIyQKrns",
  authDomain: "fincrm-a84da.firebaseapp.com",
  databaseURL: "https://fincrm-a84da.firebaseio.com",
  projectId: "fincrm-a84da",
  storageBucket: "fincrm-a84da.appspot.com",
  messagingSenderId: "861011351147"
};