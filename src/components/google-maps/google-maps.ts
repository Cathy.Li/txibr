import { GoogleMaps, GoogleMap, GoogleMapsEvent, GoogleMapOptions, MarkerOptions, MarkerIcon } from '@ionic-native/google-maps';
import { Component, Input, OnInit } from '@angular/core';
import { PopUpProvider } from '../../providers/pop-up/pop-up'
/**
 * Generated class for the GoogleMapsComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */

declare var google;

@Component({
  selector: 'google-maps',
  templateUrl: 'google-maps.html'
})
export class GoogleMapsComponent implements OnInit{

  map: GoogleMap;
  directionService = new google.maps.DirectionsService();

  @Input('origin') origin;
  @Input('destination') destination;

  constructor(
  	private popupProvider: PopUpProvider
  ){
   	this.initMap(); 
  }

  ngOnInit(){}

  initMap(){

  	this.loadMap().then((loaded)=>{
  		if (loaded) {
  			this.addMarker(this.origin).then(()=>{
  				this.addMarker(this.destination,'destination').then(()=>{
  					this.traceRoute().then((response)=>{
  						this.addPolylines(response).then((target)=>{
  							this.moveCamera(this.origin).then((ok)=>{
  								if (ok) {
  									this.moveCamera(this.destination);
  								}
  							})
  						})
  					})
  				})
  			})
  		}
  	})

  }

  loadMap(){
  	return new Promise((resolve)=>{
  		let mapOptions: GoogleMapOptions = {
  			camera: {
  				target: this.origin,
  				zoom: 18,
  				tilt: 30
  			},
  			controls: {
  				compass: false,
  				mapToolbar: true
  			}
  		};

  		this.map = GoogleMaps.create('map_canvas', mapOptions);
  		this.map.one(GoogleMapsEvent.MAP_READY).then(()=>{
  			resolve(true);
  		})
  	})

  }

  addMarker(position, type?:string){
  	return new Promise((resolve)=>{
  		let url = './assets/icon/marker.png';
  		if (type == 'destination') {
  			url = './assets/icon/destination.png';
  		}

  		let icon: MarkerIcon = {
  			url: url,
  			size: {
  				width: 40,
  				height: 40
  			}
  		}

  		let markerOption: MarkerOptions = {
  			position: {
  				lat: position.lat,
  				lng: position.lng
  			},
  			icon: icon
  		}

  		this.map.addMarker(markerOption).then((marker)=>{
  			resolve(marker);
  		})
  	})
  }

  traceRoute(){
  	return new Promise((resolve)=>{
  		let request = {
  			origin: this.origin,
  			destination: this.destination,
  			unitSystem: google.maps.UnitSystem.METRIC,
			travelMode: google.maps.TravelMode.DRIVING,

  		};

  		this.directionService.route(request, function(result, status) {
  			if (status == 'OK') {
  				resolve(result);
  			}
  		})
  	})
  }

  addPolylines(response: any){
  	return new Promise((resolve)=>{
  		let polylines = [];
  		response.routes[0].overview_path.forEach((position)=>{
  			let polyline = {lat: position.lat(), lng: position.lng()};
  			polylines.push(polyline);
  		})

  		let leg = response.routes[0].legs;

  		let start = leg.start_location;
  		let end = leg.end_location;
  		let target = [{
  			lat: start.lat(),
  			lng: start.lng()
  		}, {
  			lat: end.lat(),
  			lng: end.lng()
  		}];

  		this.map.addPolyline({
  			points: polylines,
  			color: '#b5ce93',
  			width: 10,
  			geodesic: true
  		}).then(()=>{
  			resolve(target);
  		})
  	})

  }

  moveCamera(target){
  	return new Promise((resolve)=>{
  		this.map.animateCamera({
  			target: target,
  			zoom: 18,
  			bearing: 45,
  			tilt: 90
  		});
  		resolve(true);
  	})
  }

}
