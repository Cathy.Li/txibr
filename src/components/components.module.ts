import { NgModule } from '@angular/core';
import { GoogleMapsComponent } from './google-maps/google-maps';
import { InfoLoginComponent } from './info-login/info-login';
import { PictComponent } from './pict/pict';
@NgModule({
	declarations: [GoogleMapsComponent,
    InfoLoginComponent,
    PictComponent],
	imports: [],
	exports: [GoogleMapsComponent,
    InfoLoginComponent,
    PictComponent]
})
export class ComponentsModule {}
