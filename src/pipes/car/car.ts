import { Pipe, PipeTransform } from '@angular/core';

/**
 * Generated class for the CarPipe pipe.
 *
 * See https://angular.io/api/core/Pipe for more info on Angular Pipes.
 */
@Pipe({
  name: 'carSearch',
})
export class CarPipe implements PipeTransform {
  /**
   * Takes a value and makes it lowercase.
   */
  transform(items: any[], terms: string): any[] {
    if(!items) return [];
    if(!terms) return items;
    terms = terms.toLowerCase();
    return items.filter( it => {
      return it.matricule.toLowerCase().includes(terms) || it.cartype.type.toLowerCase().includes(terms);
    });
  }
}
