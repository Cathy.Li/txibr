import { NgModule } from '@angular/core';
import { SearchPipe } from './search/search';
import { AvailableBookingPipe } from './available-booking/available-booking';
import { CarPipe } from './car/car';
@NgModule({
	declarations: [SearchPipe,
    AvailableBookingPipe,
    CarPipe],
	imports: [],
	exports: [SearchPipe,
    AvailableBookingPipe,
    CarPipe]
})
export class PipesModule {}
